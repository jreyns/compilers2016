# README #

This is the README for the compilers project of 2016 for:

* Joran Dox
* Joeri Reyns

# Dependencies #

* Python 3
* Antlr4
* Antlr4 runtime

* Extra's for running the Pmachine
    * Linux and Windows versions of the Pmachine can be found in ./Pmachine/
	* Linux (32 bit): should work out of the box
    * Linux (64 bit):  libc6:i386 libncurses5:i386 libstdc++6:i386
        (`sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386`)
    * Windows: Staticly linked exe included

(Both antlr4 and antlr4 runtime installers for windows are included in the project. Beware these are versions from the start of the project and might be outdated)

# How do I get set up? #

* Clone this git repo into a folder of your liking using the git clone command
* Move to the project folder
* Open a terminal here by shift clicking inside the folder and choosing "open terminal here" or open a new terminal and cd to this location
* Next you will have a list of possible commands you can enter

## Possible commands in the project folder ##
(.bat version for windows, .sh version for linux)

* ### build
    * Build the compiler, this will generate a build directory with all needed files in it
* ### test
    * Will do the same as build but add test files to the build directory and run all tests
* ### clean
    * Will remove the build directory

# Using the compiler #

Once you built the compiler you can perform next commands in the build directory to compile c code to p machine code

* Windows: `py -3 c2p.py source.c [target.p]`
* Linux: `python3 c2p.py source.c [target.p]`

## Extra commandline arguments are possible ##

* -h or --help, for help about commandline arguments
* (-q | --quiet) or (-s | --silent), for quiet or silent compiling respectively (sets verbosity to 0)
* (-e | --err) file, to pipe the compiler output (i.e. warnings and errors) towards a file
* (-v | --verbose) number, for changing verbose level
    * 0 : don't print anything
    * 1 : print errors and warnings
    * 2 : same as 1 with string representation of AST ,Symbol Table and error + warning count overview
    * 3 : same as 2 but print all function entries as well (only be used for compiler debugging)
    
# Supported c constructs #
## Mandatory ##

* Types
	* void
    * char
    * int
    * float
    * pointer
* Includes -> only stdio, namely printf and scanf functions
    * As a failsafe in scanf against undefined behaviour we implemented support for only single input.
	* We also chose not to support %s for usability reasons, forcing a user to enter input one character at a time seems like something you wouldn't want to do.
	* Since %s isn't supported in scanf we decided to not support it in printf for consistency. You can easily write a print array function in c which can handle printing char*
* Reserved Words
    * All types
    * if
    * else
    * while
    * return
* Variables
    * Constants
    * Local
    * Global
* Comments
    * Single-line
* Functions
    * Function declarations without parameter identifiers
    * Function definitions
    * Function calls
    * Check of return statements
	* Note function calls to main are not supported as variables arn't seen as possibly indirect within the main function
* Arrays
    * Single dimensional arrays

## Optional ##

* Reserved Words
    * for
    * const
    * continue
    * break
* Comments
    * Multi-line
* Arrays
    * Dynamic arrays
* Conversions
* Ternary operator of form `disjunction '?' simple_expression ':' condition`

# AST #

Abstract syntax tree is generated based on the src/ast.py and src/astnode.py classes
and constructed using the antlr4 visitor to visit each node in the parse tree.
We also flatten the parse tree by flattening nodes with single child since those nodes don't provide usefull information.

# Symbol Table #

The Symbol Table is an extra class defined in src/symboltable.py.
This class is used inside the AST to build the symbol table.
At every AST Node a check is made if there is a need for a new scope, new declaration, new definition, ...
This way the correctness of all statements can be tested.

# Error Listener #

For ensuring consistent error handling we chose to inherit from Antlr4's errorListeners.
This enables us to provide extra info (e.g. type warnings) to the users in the same manner as syntax errors by Antlr4.
Source code can be found in src/errorhandling.py.

#Error Analysis#

* Syntactical error analysis
    * Antlr4
* Semantical error analysis
    * Valid usage of declarations -> symbol table entries
    * Warnings based on conversions
	* Type errors and warnings
	* Valid usage of a array initializer

# Compilation to P code#

If you run the code:

* Windows: `py -3 c2p.py source.c [target.p]`
* Linux: `python3 c2p.py source.c [target.p]`

## Extra commandline arguments are possible ##

* -h or --help, for help about commandline arguments
* (-q | --quiet) or (-s | --silent), for quiet or silent compiling respectively (sets verbosity to 0)
* (-e | --err) file, to pipe the compiler output (i.e. warnings and errors) towards a file
* (-v | --verbose) number, for changing verbose level
    * 0 : don't print anything
    * 1 : print errors and warnings
    * 2 : same as 1 with string representation of AST ,Symbol Table and error + warning count overview
    * 3 : same as 2 but print all function entries as well (only be used for compiler debugging)
 	
source code will be compiled in target.p
and can then be run on the pmachine:

* ./path-to-Pmachine/Pmachine(.exe) ./path-to-target-file/target.p

#Test Cases#

Our test cases are based on the included tests of gcc
and examples found at [programmingsimplified.com](http://www.programmingsimplified.com/c-program-examples).
We also added our own test cases found in three folders:

* `src/c_code`: for the code that compiles and runs as it should
* `src/c_code_input`: for the code that compiles and runs but requires user input (and thus can't be used for automatic testing
* `src/c_code_nocompile`: for test code that shouldn't compile with gcc nor with our compiler (so comparing output with gcc is useless)

Can be run by using the test command in the project folder.

The compare_test script compiles the .c files into .p files and compares the output with the gcc's compiled version's output.

#Current platform Support#

Both Linux and Windows platforms should be supported with working clean, build and test scripts.
