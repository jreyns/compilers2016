
grammar lists;

lists	: '(' value (',' value)* ')'
		;

value   :   number
	    |   lists   // recursion
   	    |			// lambda
	    ;

number	: INT ;

INT 	:   '0' | [1-9] [0-9]* ; // no leading zeros

WS  	:   [ \t\n\r]+ -> skip ;
