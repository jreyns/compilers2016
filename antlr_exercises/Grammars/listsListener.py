# Generated from ./Grammars/lists.g4 by ANTLR 4.5.2
from antlr4 import *

# This class defines a complete listener for a parse tree produced by listsParser.
class listsListener(ParseTreeListener):

    # Enter a parse tree produced by listsParser#lists.
    def enterLists(self, ctx):
        pass

    # Exit a parse tree produced by listsParser#lists.
    def exitLists(self, ctx):
        pass


    # Enter a parse tree produced by listsParser#value.
    def enterValue(self, ctx):
        pass

    # Exit a parse tree produced by listsParser#value.
    def exitValue(self, ctx):
        pass


    # Enter a parse tree produced by listsParser#number.
    def enterNumber(self, ctx):
        pass

    # Exit a parse tree produced by listsParser#number.
    def exitNumber(self, ctx):
        pass


