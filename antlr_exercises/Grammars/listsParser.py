# Generated from ./Grammars/lists.g4 by ANTLR 4.5.2
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3")
        buf.write(u"\7\33\4\2\t\2\4\3\t\3\4\4\t\4\3\2\3\2\3\2\3\2\7\2\r\n")
        buf.write(u"\2\f\2\16\2\20\13\2\3\2\3\2\3\3\3\3\3\3\5\3\27\n\3\3")
        buf.write(u"\4\3\4\3\4\2\2\5\2\4\6\2\2\32\2\b\3\2\2\2\4\26\3\2\2")
        buf.write(u"\2\6\30\3\2\2\2\b\t\7\3\2\2\t\16\5\4\3\2\n\13\7\4\2\2")
        buf.write(u"\13\r\5\4\3\2\f\n\3\2\2\2\r\20\3\2\2\2\16\f\3\2\2\2\16")
        buf.write(u"\17\3\2\2\2\17\21\3\2\2\2\20\16\3\2\2\2\21\22\7\5\2\2")
        buf.write(u"\22\3\3\2\2\2\23\27\5\6\4\2\24\27\5\2\2\2\25\27\3\2\2")
        buf.write(u"\2\26\23\3\2\2\2\26\24\3\2\2\2\26\25\3\2\2\2\27\5\3\2")
        buf.write(u"\2\2\30\31\7\6\2\2\31\7\3\2\2\2\4\16\26")
        return buf.getvalue()


class listsParser ( Parser ):

    grammarFileName = "lists.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"'('", u"','", u"')'" ]

    symbolicNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                      u"INT", u"WS" ]

    RULE_lists = 0
    RULE_value = 1
    RULE_number = 2

    ruleNames =  [ u"lists", u"value", u"number" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    INT=4
    WS=5

    def __init__(self, input):
        super(listsParser, self).__init__(input)
        self.checkVersion("4.5.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ListsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(listsParser.ListsContext, self).__init__(parent, invokingState)
            self.parser = parser

        def value(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(listsParser.ValueContext)
            else:
                return self.getTypedRuleContext(listsParser.ValueContext,i)


        def getRuleIndex(self):
            return listsParser.RULE_lists

        def enterRule(self, listener):
            if hasattr(listener, "enterLists"):
                listener.enterLists(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitLists"):
                listener.exitLists(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitLists"):
                return visitor.visitLists(self)
            else:
                return visitor.visitChildren(self)




    def lists(self):

        localctx = listsParser.ListsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_lists)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 6
            self.match(listsParser.T__0)
            self.state = 7
            self.value()
            self.state = 12
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==listsParser.T__1:
                self.state = 8
                self.match(listsParser.T__1)
                self.state = 9
                self.value()
                self.state = 14
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 15
            self.match(listsParser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ValueContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(listsParser.ValueContext, self).__init__(parent, invokingState)
            self.parser = parser

        def number(self):
            return self.getTypedRuleContext(listsParser.NumberContext,0)


        def lists(self):
            return self.getTypedRuleContext(listsParser.ListsContext,0)


        def getRuleIndex(self):
            return listsParser.RULE_value

        def enterRule(self, listener):
            if hasattr(listener, "enterValue"):
                listener.enterValue(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitValue"):
                listener.exitValue(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitValue"):
                return visitor.visitValue(self)
            else:
                return visitor.visitChildren(self)




    def value(self):

        localctx = listsParser.ValueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_value)
        try:
            self.state = 20
            token = self._input.LA(1)
            if token in [listsParser.INT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 17
                self.number()

            elif token in [listsParser.T__0]:
                self.enterOuterAlt(localctx, 2)
                self.state = 18
                self.lists()

            elif token in [listsParser.T__1, listsParser.T__2]:
                self.enterOuterAlt(localctx, 3)


            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class NumberContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(listsParser.NumberContext, self).__init__(parent, invokingState)
            self.parser = parser

        def INT(self):
            return self.getToken(listsParser.INT, 0)

        def getRuleIndex(self):
            return listsParser.RULE_number

        def enterRule(self, listener):
            if hasattr(listener, "enterNumber"):
                listener.enterNumber(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitNumber"):
                listener.exitNumber(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitNumber"):
                return visitor.visitNumber(self)
            else:
                return visitor.visitChildren(self)




    def number(self):

        localctx = listsParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_number)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 22
            self.match(listsParser.INT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





