import sys
from antlr4 import *
from listsListener import listsListener
from listsVisitor import listsVisitor
from listsLexer import listsLexer
from listsParser import listsParser

class KeyPrinter(listsListener):
	def exitNumber(self, ctx):
		print ctx.INT(), # trailing comma = no newline cuz reasons

class NumberPrinter(listsVisitor):
	# Visit a parse tree produced by listsParser#lists.
	def visitLists(self, ctx):
		print "entered list"
		print dir(ctx)
		return self.visitChildren(ctx)

	# Visit a parse tree produced by listsParser#value.
	def visitValue(self, ctx):
		print "entered value"
		print dir(ctx)
		return self.visitChildren(ctx)

	# Visit a parse tree produced by listsParser#number.
	def visitNumber(self, ctx):
		print "entered number"
		print dir(ctx)
		print ctx.INT()
		return self.visitChildren(ctx)
	

def main(argv):
	input = FileStream(argv[1])
	lexer = listsLexer(input)
	stream = CommonTokenStream(lexer)
	parser = listsParser(stream)
	tree = parser.lists()

	# initial stuff, works
	"""
	printer = KeyPrinter()
	walker = ParseTreeWalker()
	walker.walk(printer, tree)
	print 
	"""
	
	# poging 2
	printer2 = NumberPrinter()
	printer2.visit(tree)
	print


if __name__ == '__main__':
	main(sys.argv)
