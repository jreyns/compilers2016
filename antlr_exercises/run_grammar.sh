#!/bin/bash

file=$1
echo "Generating Python classes for $file:"

java -jar ../antlr/antlr-4.5.2-complete.jar -Dlanguage=Python2 ./Grammars/$file -visitor

echo "Done"
