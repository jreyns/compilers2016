@echo off

if not exist ".\build" mkdir .\build

echo Copying build files:
copy /y .\src\* .\build

cd .\build

set file=../grammar/smallC.g4

echo Generating Python classes for %file%:

java -jar ../antlr/antlr-4.5.2-complete.jar -Dlanguage=Python3 %file% -visitor

cd ..

echo Done
