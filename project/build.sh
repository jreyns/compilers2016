#!/bin/bash


if [ ! -d "./build" ]; then
  mkdir ./build
fi 

echo Copying build files:
cp ./src/* ./build
cp ./grammar/smallC.g4 ./build

cd ./build

echo Generating Python classes for smallC.g4:

java -jar ../antlr/antlr-4.5.2-complete.jar -Dlanguage=Python3 smallC.g4 -visitor

cd ..

echo Done
