#include <stdio.h>

int acker(int, int);

int
main()
{
    int n = acker(2,2);
    printf("acker(2,2) = %i\n", n);
    return(0);
}

int
acker(int x,int y)
{
    printf("acker(%i,%i)\n", x, y);
    if (x==0)
	return(y+1);
    else if (y==0)
	return(acker(x-1,1));
    else
	return(acker(x-1, acker(x, y-1)));
}
