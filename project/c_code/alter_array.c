#include <stdio.h>

void alterArray(int a[10]){
    int i = 0;
    while(i < 10){
        a[i] = i;
        i++;
    }
}

int main()
{
    int a[10] = {0,0,0,0,0,0,0,0,0,0};
    
    alterArray(a);
    
    int i = 0;
    while(i < 10){
        printf("%i\n", a[i]);
        i++;
    }

    return 0;
}