#include <stdio.h>

int test[] = {'a','b','c','d','e','f','g','h','i','j'};
int test1[5] = {'a','b','c','d','e','f','g','h','i','j'};

void useArrayInt(int array[10]){
	int i = 0;
	int tmp;
	while(i < 10){
		tmp = array[i];
		printf("%i ", tmp);
		i++;
	}
}

void useArrayChar(char array[10]){
	int i = 0;
	int tmp;
	while(i < 10){
		tmp = array[i];
		printf("%i ", tmp);
		i++;
	}
}

int main()
{
	int test2[10];
	for(int i = 0; i < 10; i++){
		test2[i] = test[i];
	}
    int array[10] = {'a','b','c','d','e','f','g','h','i','j'};
	char string[] = "Hello World";
	useArrayChar(array);
	printf("\n");
	useArrayInt(array);
	printf("\n");
	useArrayChar(string);
	printf("\n");
	useArrayInt(string);
	printf("\n");
    return 0;
}
