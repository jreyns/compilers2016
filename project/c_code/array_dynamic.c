#include <stdio.h>

void init(int* arr, int size){
    for(int i = 0; i < size; i++){
        arr[i] = 0;
    }
}

void printarray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%i ", arr[i]);
    }
    printf("\n");
}

int sum(int* arr, int size){
    int ret = 0;
    for(int i = 0; i < size; i++){
        ret += arr[i] * 2;
    }
    return ret;
}

void alterArrayFor(int* arr, int size){
    for(int i = 0; i < size; i++){
        if(i % 2 == 0){
            arr[i] = i;
            continue;
        }
        else if(i % (size - 3) == 0){
            arr[i] = sum(arr, size);
            break;
        }else{
            continue;
        }
        arr[i] = i;
    }
}

void alterArrayWhile(int* arr, int size){
    int i = 0;
    while(i < size){
        if(i % 2 == 0){
            arr[i] = i;
            i++;
            continue;
        }
        else if(i % (size - 3) == 0){
            arr[i] = sum(arr, size);
            break;
        }else{
            i++;
            continue;
        }
        arr[i] = i;
        i++;
    }
}

int main()
{
    int i = 10;
    int j = 20;
    
    int arrayFor[i];
    int arrayWhile[j];
    
    init(arrayFor, i);
    init(arrayWhile, j);
    
    alterArrayFor(arrayFor, i);
    alterArrayWhile(arrayWhile, j);
    
    // 0 0 2 0 4 0 6 24 0 0
    printarray(arrayFor, i);
    // 0 0 2 0 4 0 6 0 8 0 10 0 12 0 14 0 16 144 0 0
    printarray(arrayWhile, j);

    return 0;
}

