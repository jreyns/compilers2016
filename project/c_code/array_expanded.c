#include <stdio.h>

void printarray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%i", arr[i]);
    }
    printf("\n");
}

void arraytest1(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        arr[i] += 1;
    }
}


void arraytest2(int arr[5], int size) {
    for (int i = 0; i < 5; i++) {
        arr[i] += 1;
    }
}

void arraytest3(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        arr[i] += 1;
    }
}

int main()
{
    int arr[5] = {0, 1, 0, 2, 0};
    printf("Hello, World!\n");
    printarray(arr, 5);
    arraytest1(arr, 5);
    printarray(arr, 5);
    arraytest2(arr, 5);
    printarray(arr, 5);
    arraytest3(arr, 5);
    printarray(arr, 5);

    return 0;
}

