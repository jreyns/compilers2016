#include <stdio.h>

void init(int* arr, int size){
    for(int i = 0; i < size; i++){
        arr[i] = 0;
    }
}

void printarray(int* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%i ", arr[i]);
    }
    printf("\n");
}

int main()
{
    int i = 6;
    int array[i];
    init(array, i);
    int *j = array;
    *j = *j + 1;
    array[1]++;
    array[2]--;
    ++array[3];
    --array[4];
    int *k = &array[5];
    *k = 5;
    // expected output 1 1 -1 1 -1 5
    printarray(array, i);

    return 0;
}