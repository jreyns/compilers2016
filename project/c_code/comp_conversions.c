#include <stdio.h>

int main()
{
    int li = 9;
    float lf = 9.03;
    char lc = '9';
    
    int si = 3;
    float sf = 3.03;
    char sc = '3';


    printf("lf > sf ? %i\n", lf > sf);
    printf("lf > si ? %i\n", lf > si);
    printf("lf > sc ? %i\n", lf > sc);
    
    printf("li > sf ? %i\n", li > sf);
    printf("li > si ? %i\n", li > si);
    printf("li > sc ? %i\n", li > sc);
    
    
    printf("lc > sf ? %i\n", lc > sf);
    printf("lc > si ? %i\n", lc > si);
    printf("lc > sc ? %i\n", lc > sc);
    
    return 0;
}
