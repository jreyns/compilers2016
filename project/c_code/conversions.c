#include <stdio.h>

int takesi(int i) {
    printf("takesi %i\n", i);
    return i;
}

float takesf(float f) {
    printf("takesf %f\n", f);
    return f;
}

char takesc(char c) {
    printf("takesc %c\n", c);
    return c;
}

char takesfbutreturnsc(float f) {
        return f;
}

char takesibutreturnsc(int i) {
        return i;
}

int takescbutreturnsi(char c) {
        return c;
}

int takesfbutreturnsi(float f) {
        return f;
}

float takescbutreturnsf(char c) {
        return c;
}

float takesibutreturnsf(int i) {
        return i;
}

int main()
{
    int i = 30;
    float f = 30.03;
    char c = '3';

    takesi(i);
    takesi(f);
    takesi(c);
    takesf(i);
    takesf(f);
    takesf(c);
    takesc(i);
    takesc(f);
    takesc(c);

    printf("f -> c %c\n", takesfbutreturnsc(f));
    printf("i -> c %c\n", takesibutreturnsc(i));
    printf("c -> i %i\n", takescbutreturnsi(c));
    printf("f -> i %i\n", takesfbutreturnsi(f));
    printf("c -> f %f\n", takescbutreturnsf(c));
    printf("i -> f %f\n", takesibutreturnsf(i));

    return 0;
}
