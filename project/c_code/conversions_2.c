#include <stdio.h>

int main()
{
    int i = 5;
    float f = 3.5;
    
    printf("i / f = %i\n", i / f);
    printf("i / f = %f\n", i / f);
    
    printf("i * f = %i\n", i * f);
    printf("i * f = %f\n", i * f);
    
    printf("i + f = %i\n", i + f);
    printf("i + f = %f\n", i + f);
    
    printf("i - f = %i\n", i - f);
    printf("i - f = %f\n", i - f);
    
    
    printf("f / i = %i\n", f / i);
    printf("f / i = %f\n", f / i);
    
    printf("f * i = %i\n", f * i);
    printf("f * i = %f\n", f * i);
    
    printf("f + i = %i\n", f + i);
    printf("f + i = %f\n", f + i);
    
    printf("f - i = %i\n", f - i);
    printf("f - i = %f\n", f - i);    

    return 0;
}

