#include <stdio.h>

int main()
{
    int x = 10;
    int tot = x;
    while(x != 1){
        x--;
        tot = tot * x;
    }
	// expected values x = 1 and tot = 3628800
    printf("x: %i, tot: %i \n", x, tot);
    return 0;
}