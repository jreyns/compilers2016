#include <stdio.h>

int f1(int i) {
    int j = 1;
    printf("F1: %i\n", i);
    return i + j;
}

int f2(int i) {
    int j = 2;
    printf("F2: %i\n", i);
    return f1(i * j);
}

int f3(int i) {
    int j = 3;
    printf("F3: %i\n", i);
    return f2(i - j);
}

int main()
{
    int i = 6;
    printf("main %i\n", i);

	i = f3(i);
    printf("main %i\n", i);

    return 0;
}

