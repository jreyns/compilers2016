#include <stdio.h>

int doubleN(int n){
    int ret = n + n;
    return ret;
}

int power(int n){
    int ret = n * n;
    return ret;
}

int powerOfDouble(int n){
    int ret = power(doubleN(n));
    return ret;
}

int main()
{
    int base = 5;
    int d = doubleN(base);
    int p = power(base);
    int power_of_double = powerOfDouble(base);
    
    // Expected value of 5 10 25 100
    printf("%i %i %i %i\n", base, d, p, power_of_double);

    return 0;
}
