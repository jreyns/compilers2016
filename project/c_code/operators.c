#include <stdio.h>

int main()
{
    int a = 4;
    int b = 2;

    int c;

    c = a + b;
    if (c != 6)
        printf("Error @ + \n");

    c = a - b;
    if (c != 2)
        printf("Error @ - \n");

    c = a * b;
    if (c != 8)
        printf("Error @ * \n");

    c = a / b;
    if (c != 2)
        printf("Error @ / \n");

    c = a % b;
    if (c != 0)
        printf("Error @ % \n");

    c = a;
    c += b;
    if (c != 6)
        printf("Error @ += \n");

    c = a;
    c -= b;
    if (c != 2)
        printf("Error @ -= \n");

    c = a;
    c *= b;
    if (c != 8)
        printf("Error @ *= \n");

    c = a;
    c /= b;
    if (c != 2)
        printf("Error @ /= \n");

    c = a + b;
    if (c != 6)
        printf("Error @ + \n");


    return 0;
}


