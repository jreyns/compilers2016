#include <stdio.h>

int main()
{
    int a = 4;
    int b = 2;

    int c = 0;

    c = a + b;
    if (c != 6)
        printf("Error @ + \n");

    c = 0;
    c = a - b;
    if (c != 2)
        printf("Error @ - \n");

    c = 0;
    c = a * b;
    if (c != 8)
        printf("Error @ * \n");

    c = 0;
    c = a / b;
    if (c != 2)
        printf("Error @ / \n");

    c = a;
    c = a % b;
    if (c != 0)
        printf("Error @ % \n");

    c = a;
    c += b;
    if (c != 6)
        printf("Error @ += \n");

    c = a;
    if (c != 4)
        printf("Error @ = \n");

    c = a;
    c -= b;
    if (c != 2)
        printf("Error @ -= \n");

    c = a;
    c *= b;
    if (c != 8)
        printf("Error @ *= \n");

    c = a;
    c /= b;
    if (c != 2)
        printf("Error @ /= \n");

    c = a;
    c %= b;
    if (c != 0)
        printf("Error @ % \n");

    c = a;
    c = ++c;
    if (c != 5)
        printf("Error @ ++c \n");


    c = a;
    c = --c;
    if (c != 3)
        printf("Error @ --c \n");

    c = a;
    c = c++;
    if (c != 4)
        printf("Error @ c++ \n");


    c = a;
    c = c--;
    if (c != 4)
        printf("Error @ c-- \n");

    c = 0;
    c = -a;
    if (c != -4)
        printf("Error @ -a \n");

    c = 0;
    c = a && b;
    if (c != 1)
        printf("Error @ && \n");

    c = 0;
    c = a || b;
    if (c != 1)
        printf("Error @ || \n");

    c = 0;
    c = a == b;
    if (c != 0)
        printf("Error @ == \n");

    c = 0;
    c = a != b;
    if (c != 1)
        printf("Error @ != \n");

    c = 0;
    c = a < b;
    if (c != 0)
        printf("Error @ < \n");

    c = 0;
    c = a > b;
    if (c != 1)
        printf("Error @ > \n");

    c = 0;
    c = a <= b;
    if (c != 0)
        printf("Error @ <= \n");

    c = 0;
    c = a >= b;
    if (c != 1)
        printf("Error @ >= \n");

    return 0;
}


