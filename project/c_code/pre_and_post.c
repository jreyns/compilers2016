#include <stdio.h>

int main()
{
    int i = 1;
    int j = 1;
    int* k = &i;
    int* l = k;
    
	//should print pre: 2\n post: 1\n
    printf("pre: %i\npost: %i\n", ++i, j++);
	//should print pre: 1\n post: 2\n
    printf("pre: %i\npost: %i\n", --i, j--);
    
    *k = i++;
	//should print 2\n
    printf("%i\n", *k);
    
    *l = ++j;
	//should print 2\n
    printf("%i\n", *l);
    
    *k++ = i--;
    printf("%i\n", *(--k));
    printf("%i %i\n", i, *k);

    return 0;
}