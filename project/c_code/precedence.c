#include <stdio.h>

int main()
{
    int true = (1 == 1);
    int false = (1 != 1);
    int a = true && false;
    printf("%i\n", true);
    int b = true || false;
    printf("%i\n", false);
    
    int two = 2;
    printf("%i\n", two);
    int three = 3;
    printf("%i\n", three);
    int six = 6;
    printf("%i\n", six );
    int f = two * three == six ; // regular, no brackets => == gets precedence over *
    printf("%i\n", f);
    int g = (two * three) == six ; // left to right
    printf("%i\n", g);
    int h = two * (three == six ); // right to left
    printf("%i\n", h);
    

    int i = six == two * three; // regular, no brackets => == gets precedence over *
    printf("%i\n", i);
    int j = (six == two) * three;
    printf("%i\n", j);
    int k = six == (two * three);
    printf("%i\n", k);

    int l = two && three; // regular, no brackets => == gets precedence over *
    printf("%i\n", l);
    int m = two || three;
    printf("%i\n", m);
    int n = six && (two || three);
    printf("%i\n", n);

    int thebigtest = six * true + true || false * three && two + (six / three == two);
    printf("%i\n", thebigtest);

    thebigtest = three * (six * true + true || false) * three && two + (six / three == two);
    printf("%i\n", thebigtest);
    
    thebigtest = six * true + (true || false * three && two) + (six / three == two);
    printf("%i\n", thebigtest);


    printf("Hello, World!\n");

    return 0;
}

