#include <stdio.h>

int first = 1;

int main(int argc)
{
    if(first) {
        first = 0;
	main(0);
    } else if(argc < 10) {
        main(argc+1);
	printf("Hello, World! %i\n", argc);
    }
    return 0;
}

