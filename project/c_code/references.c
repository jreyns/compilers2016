#include <stdio.h>

int main()
{
    int i = 2;
    int* j = &i;
    *j = 5;
    printf("i = %i\n", i);
    printf("j = %i\n", j);
    printf("j refers to %i\n", *j);
    return 0;
}

