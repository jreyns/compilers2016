#include <stdio.h>

void printarray(char* arr, int size) {
    for (int i = 0; i < size; i++) {
        printf("%c", arr[i]);
    }
    printf("\n");
}

int main()
{
	char success[] = "Correct, i should be smaller than j";
	char fail[] = "Incorrect, i should be smaller than j";
    int i = 5;
    int j = 6;
    int result = i < j ? 1 : 0;
    // expected output: "Correct, i should be smaller than j"
	if(result == 1){
	    printarray(success, 35);
	}else{
		printarray(fail, 37);
	}
    

    return 0;
}