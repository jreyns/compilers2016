#include <stdio.h>
 
int main(int test)
{
  int n, i, c, a = 1;
 
  printf("Enter the number of rows of Floyd's triangle to print\n");
  scanf("%i", &n);
 
  i = 1;
  
  while ( i <= n)
  {
	c = 1;
    while (c <= i)
    {
      printf("%i ",a);
      a++;
	  c++;
    }
    printf("\n");
	i++;
  }
 
  return 0;
}
