#include <stdio.h>

int main()
{
	const int i = 5;
	int *j = &i;
	*j = 6;
	i = 7;
    printf("%i\n", i);

    return 0;
}