#include <stdio.h>

int main()
{
    int i = 5;
    int* j = &i;
    
    //should all give errors or possible segfault
    *i = 4;
    &i = 3;
    j = *i;
    j = &i++;
    j = &i--;
    ++j = &i;
    j-- = &i;

    return 0;
}