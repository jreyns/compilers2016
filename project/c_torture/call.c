/* { dg-skip-if "requires untyped assembly" { ! untyped_assembly } { "-O0" } { "" } } */

int foo () {}

int main (int a, int b)
{
  foo (foo (a, b), foo (b, a));
  return 0;
}
