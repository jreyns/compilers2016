/* { dg-require-effective-target untyped_assembly } */
int g1, g2;

void
write_at (int *addr, int off, int val)
{
  g2 = 1;
  addr[off] = val;
  g2++;
}

int main ()
{
  g2 = 12;
  write_at (&g1, &g2 - &g1, 12345);
  printf ("%d\n", g2);
}
