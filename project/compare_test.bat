@echo off

call .\clean.bat
call .\build.bat

if not exist ".\build\c" mkdir .\build\c

copy /y .\c_code\* .\build\c
copy /y .\Pmachine\Pmachine.exe .\build

cd .\build

for %%f in (.\c\*.c) do (
	echo Compiling %%~f to Pmachine code
	py -3 c2p.py %%~f %%~f.p %*
	echo Running %%~f.p on the Pmachine
	Pmachine.exe %%~f.p > %%~f.p.output
	echo comparing file outputs:
	FC %%~f.p.output %%~f.exe.output
)

cd ..
