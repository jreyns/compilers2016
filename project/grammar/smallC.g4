grammar smallC;

//==========================================================================================================================
//==========================================================================================================================
// LEXER RULES
//==========================================================================================================================
//==========================================================================================================================

	// Basic patterns
		DIGIT 					: [0-9];
		ESCAPED 				: '\\"';
		FORMAT_SPECIFIERS 		: '%d' | '%i' | '%s' | '%c';
		STRING 					: '"' (FORMAT_SPECIFIERS | ESCAPED | ~('"'))*? '"';
		IDENTIFIER				: ('_' | CHARACTER) ('_' | CHARACTER | DIGIT)*;
		CHARACTER 				: [a-zA-Z];
		CHAR 					: '\'' (. | '\\' . ) '\'';
		
		
		
	// Whitespace and comments
		WHITESPACE 				: ( ' ' | '\t' | '\u000C') -> skip;
		SINGLELINE_COMMENT 		: '//' ~('\r' | '\n')* '\r'? '\n'? -> skip;
		MULTILINE_COMMENT 		: ( '/*' .*? '*/' ) -> skip;
		NEWLINE 				: ('\r'? '\n') -> skip;

//==========================================================================================================================
//==========================================================================================================================
// GRAMMAR RULES
//==========================================================================================================================
//==========================================================================================================================

	// Main file
		program 				: (include | declaration)* EOF;

		
	// Includes can either be #include <file> or #include "file" where file is a string
		include 				: '#include' '<' 'stdio.h' '>'
								;
							
	// Declaration can either be a variable or function declaration
		declaration 			: type_specifier (variable_declaration (',' variable_declaration)* ';' | function_definition | function_declaration)
								;
								
								
	// List of variable declarations e.g. int i,j=0,k;
		variable_declaration 	: basetype? (variable_definition | variable_identifier)
								;

		
	// Variables are declared and defined and often initialized
		variable_definition 	: variable_identifier '=' simple_expression
								;
								
		
	// All which should be known for functions, their declarations, their definitions or their calls
		function_definition		: basetype? identifier '(' parameter_list ')' compound_statement;
		
		function_declaration	: basetype? identifier '(' parameter_decl_list ')' ';';
	
		function_call 			: identifier '(' expression_list ')' 
								| read_statement
								| print_statement
								;

	
	// A function can have none, one or multiple parameter declarations
		parameter_list  		: 
								| type_specifier basetype? identifier array_index? (',' type_specifier basetype? identifier )* 
								;
								
		parameter_decl_list 	: 
								| type_specifier basetype? (identifier array_index?)? (',' type_specifier basetype? (identifier)? )* 
								;
				
		
	// A structure to specify lifetime for all variables declared inside this block
		compound_statement  	: '{' (statement)* '}';
		
		
	// The well known IF test
		conditional_statement 	: 'if' '(' expression ')' statement ('else' statement)?;

		
	// The While loop
		while_statement 		: 'while' '(' expression ')' statement;
		
	// The For loop
		for_statement			: 'for' '(' (expression ';' | declaration)  expression ';' expression ')' statement;
		
		
	// The Return statement
		return_statement 		: 'return' expression ';';

		
	// The read statement, in this case should resemble scanf function
		read_statement 			: 'scanf' '(' string ',' (variable)? (',' variable)* ')' ';';
		
	// The print statement, in this case should resemble printf function
		print_statement 		: 'printf' '(' string (',' (expression | function_call) )*? ')' ';';
		
		
	// A compound statement consists of possible many statements
		statement 				: continue_statement
								| break_statement
								| return_statement 
								| function_call 
								| conditional_statement 
								| while_statement 
								| for_statement
								| expression ';'
								| declaration
								| compound_statement
								;
								
		continue_statement		: 'continue' ';';
		break_statement			: 'break' ';';

								
	// Provided parameters for a function can be resolved by literals, variables or expressions in general
		expression_list 		: expression ( ',' expression)*?;


	// An expression is a lable for grouping the possibilities assignement, more simplistic expression or declaration
		expression 				: assignment
								| simple_expression 
								| empty
								;
								
		assignment				: variable assignment_operator simple_expression;
		
		assignment_operator		: '='
								| '+='
								| '-='
								| '*='
								| '/='
								| '%='
								;
								
		empty					: ;

		
	// Simple expressions can be type casts, conditions, variables or array initialisations
		simple_expression 		: type_cast
								| condition
								| variable_identifier
								| array_init
								;
								
	// Root node of any conditional subtree
		condition 				: disjunction
								| disjunction '?' simple_expression ':' condition
								;

								
	// A list of OR statments
		disjunction 			: conjunction ('||' conjunction)*;

		
	// A list of AND statements
		conjunction 			: comparison ('&&' comparison)*;

		
	// A comparison which results in a boolean value
		comparison 				: relation (comparison_operator relation)?;

		
	// Compairison operators
		comparison_operator 	: '=='
								| '!='
								;

								
	// A relation which results in a boolean value
		relation 				: summation (relational_operator relation)?;

		
	// Relational operators
		relational_operator 	: '<' 
								| '>' 
								| '<=' 
								| '>='
								;

								
	// Basic blueprint of a sum, a set of terms added or subtracted
		summation 				: term (term_operator summation)?;

		
	// Term operators
		term_operator 			: '+' 
								| '-'
								;

								
	// Each term is a set of factors seperated by factor operators mul div and mod
		term 					: factor (factor_operator term)?;

		
	// Operators for factors
		factor_operator 		: '*' 
								| '/' 
								| '%'
								;

								
	// Time for the first order operators negation and sign
		factor 					: unary_operator factor 
								| atom
								;

		unary_operator			: '!'
								| '-'
								;
								
	// Atomic values of which factors concise
		atom 					: integer
								| floating_point
								| char
								| variable 
								| function_call 
								| '(' expression ')' 
								| string
								;
								
		integer					: DIGIT+;
		
		floating_point			: DIGIT* '.' DIGIT+;
		
		char					: CHAR;
								
								
	// Variable, can be value, pointer, reference, increment or decrement
		variable				: pre? variable_identifier post?
								;
								
	// Symbols that can be found pre variable
		pre						: '++'
								| '*'
								| '&'
								| '--'
								;
								
								
	// Symbols that can be found post variable
		post					: '++'
								| '--'
								;
								
								
	// The ability to index an array
		array_index 			: '[' (simple_expression | empty) ']';

		
	// The ability to initialise an array based on its {,} representation
		array_init 				: '{' simple_expression (',' simple_expression)* '}';

		
	// All supported types for this compiler
		type_specifier 			:('const')? 
								( 
								'void' 
								| 'int' 
								| 'char' 
								| 'float' 
								);

								
	// An expression can also contain type casts
		type_cast 				: '(' type_specifier basetype? ')' simple_expression;

		
	// Single variable or array element. TODO think about dereferencing
		variable_identifier 	: '(' variable ')'
								| identifier array_index?
								;
		
	// Basetype either value = standard or pointer
		basetype				: '*';
	
		
	// The actual identifier
		identifier 				: IDENTIFIER;
		
	// String
		string					: STRING;
		

	