grammar smallC;

//==========================================================================================================================
//==========================================================================================================================
// GRAMMAR RULES
//==========================================================================================================================
//==========================================================================================================================

	// Main file
		program 				: sub_element*;

		
	// Sub elements which can be in files: typedefs, includes, funtions and declarations
		sub_element 			: typedef ';'
								| '#include' include 
								| function 
								| declaration ';'
								;

								
	// Type definitions
		typedef 				: 'typedef' type_specifier identifier;

		
	// Includes can either be #include <file> or #include "file" where file is a string
		include 				: '<' stdio '>'  
								| '"stdio.h"'
								;
								
		stdio					:	'stdio.h'
								|	'stdio'
								;
							
	// Declaration can either be a variable or function declaration
		declaration 			: type_specifier variable_declaration
								| function_declaration;

								
	// List of variable declarations e.g. int i,j=0,k;
		variable_declaration 	: variable_definition (',' variable_definition)*;

		
	// Variables are declared and defined and often initialized
		variable_definition 	: variable_identifier ('=' expression)?;

		
	// All which should be known for functions, their declarations, their definitions or their calls
		function_declaration 	: type_specifier identifier '(' parameter_list ')';
		
		function 				: function_declaration compound_statement;
	
		function_call 			: identifier '(' expression_list? ')';

		
	// Provided parameters for a function can be resolved by literals, variables or expressions in general
		expression_list 		: expression ( ',' expression_list)?;

		
	// All supported types for this compiler
		type_specifier 			:('const')? 
								( 
								'void' 
								| 'int' 
								| 'char' 
								| 'float' 
								| '*' 
								| identifier
								);

								
	// An expression can also contain type casts
		type_cast 				: '(' type_specifier ')' variable_identifier;

		
	// A function can have none, one or multiple parameter declarations
		parameter_list  		: 
								| parameter_declaration (',' parameter_declaration )* 
								;

		parameter_declaration 	: type_specifier identifier;

		
	// A structure to specify lifetime for all variables declared inside this block
		compound_statement  	: '{' (statement)* '}';

		
	// The well known IF test
		conditional_statement 	:  'if' '(' expression ')' statement ('else' statement)?;

		
	// The While loop
		while_statement 		: 'while' '(' expression ')' statement;

		
	// The Return statement
		return_statement 		: 'return' (simple_expression)?;

		
	// The read statement, in this case should resemble scanf function
		read_statement 			: 'scanf' '(' STRING ',' variable_list ')';

		
	// The print statement, in this case should resemble printf function
		print_statement 		: 'printf' '(' STRING (',' (expression | function_call) )* ')';
		
		
	// A compound statement concists of possible many statements
		statement 				: typedef ';'
								| 'continue' ';'
								| 'break' ';'
								| return_statement ';'
								| function_call ';'
								| conditional_statement 
								| while_statement 
								| compound_statement 
								| expression ';'
								| read_statement ';' 
								| print_statement ';' 
								;

	// An expression is a lable for grouping the possibilities assignement, more simplistic expression or declaration
		expression 				: assignment 
								| simple_expression 
								| declaration
								;

								
	// Assignement
		assignment 				: variable_identifier '=' simple_expression;

		
	// Simple expressions can be type casts, conditions, variables or array initialisations
		simple_expression 		: type_cast
								| condition
								| variable_identifier
								| array_init;

								
	// The ability to index an array
		array_index 			: '[' simple_expression ']';

		
	// The ability to initialise an array based on its {,} representation
		array_init 				: '{' expression (',' expression)* '}';

		
	// Root node of any conditional subtree
		condition 				: disjunction 
								| disjunction '?' expression ':' condition
								;

								
	// A list of OR statments
		disjunction 			: conjunction ('||' conjunction)*;

		
	// A list of AND statements
		conjunction 			: comparison ('&&' comparison)*;

		
	// A comparison which results in a boolean value
		comparison 				: relation (comparison_operator relation)?;

		
	// Compairison operators
		comparison_operator 	: '=='
								| '!='
								;

								
	// A relation which results in a boolean value
		relation 				: summation (relational_operator relation)?;

		
	// Relational operators
		relational_operator 	: '<' 
								| '>' 
								| '<=' 
								| '>='
								;

								
	// Basic blueprint of a sum, a set of terms added or subtracted
		summation 				: term (term_operator summation)?;

		
	// Term operators
		term_operator 			: '+' 
								| '-'
								;

								
	// Each term is a set of factors seperated by factor operators mul div and mod
		term 					: factor (factor_operator term)?;

		
	// Operators for factors
		factor_operator 		: '*' 
								| '/' 
								| '%'
								;

								
	// Time for the first order operators negation and sign
		factor 					: '!' factor 
								| '-' factor 
								| atom
								;

								
	// Atomic values of which factors concise
		atom 					: DIGIT+
								| DIGIT* '.' DIGIT+
								| variable_identifier 
								| function_call 
								| '(' expression ')' 
								| STRING
								;

								
	// List of variables
		variable_list 			: variable_identifier (',' variable_identifier)*;

		
	// Single variable or array element. TODO think about dereferencing
		variable_identifier 	: identifier (array_index)?;

		
	// The actual identifier
		identifier 				: ('_' | CHARACTER) ('_' | CHARACTER | DIGIT)*;
	
//==========================================================================================================================
//==========================================================================================================================
// LEXER RULES
//==========================================================================================================================
//==========================================================================================================================

	DIGIT : [0-9];
	CHARACTER : [a-zA-Z];
	STRING : '"' .* '"';
		
	// Whitespace and comments
		WHITESPACE : ( ' ' | '\t' | '\u000C') -> skip;
		SINGLELINE_COMMENT : '//' ~('\r' | '\n')* '\r'? '\n'? -> skip;
		MULTILINE_COMMENT : ( '/*' .* '*/' ) -> skip;
		NEWLINE : ('\r'? '\n') -> skip;