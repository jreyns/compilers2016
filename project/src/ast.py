
from astnode import *
		
		
class AST():
	
	def __init__(self, errorListener):
		# Added verbose levels for debugging
		printVerbose(errorListener.verbose, 3, "AST.__init__()", errorListener.file)
		
		self.root = ASTNode(None, 0, 0, errorListener)
		self.nodes = [[self.root]]
		self.scope = Scope("program")
		self.errorListener = errorListener
		
	def __repr__(self):
		'''
			Function that gets called by print
			
			Simply print the tree starting from the root
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.__repr__()", self.errorListener.file)
		
		return str(self.root)
		
	def setRoot(self, start, type, content):
		'''
			Function to allow root construction in the AST
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.setRoot()", self.errorListener.file)
		
		self.root.setStart(start)
		self.root.setType(type)
		self.root.setContent(content)
		self.root.scope = self.scope

	def addChild(self, depth, start, type, content):
		'''
			Function which adds a child to the AST at a certain depth
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.addChild()", self.errorListener.file)
		
		# depth of the parent node
		depth = depth - 1
		
		# nodes should be added at the right depth, so add a new one if the depth isn't available yet
		if(len(self.nodes) == depth):
			self.nodes.append([])
			
		# parent node
		parent = self.nodes[depth - 1][-1]
		
		# add the child
		child = parent.addChild(depth, len(self.nodes[depth]), type, content, start)
		
		# add the child to the AST
		self.nodes[depth].append(child)
		
		# return the constructed child node
		return child
		
	def printTree(self):
		'''
			print function
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.printTree()", self.errorListener.file)
		
		return self.__repr__()
		
	def addScopes(self, node, scope):
		'''
			Recursive function which allows the addition of symbol table entries at the right depth
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.addScopes()", self.errorListener.file)
		
		# collection of child nodes that still have to be added to the symbol table
		to_add = node.addScope(scope)
		
		# recursive call
		for child in to_add:
			self.addScopes(child, node.scope)
			
	def checkReturnValues(self, node):
		'''
			Recursive function which allows for checking the return statements in function_definitions
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.checkReturnValues()", self.errorListener.file)
		
		try:
			# collection of child nodes that still have to be added to the symbol table
			if node.type == "function_definition":
				node.checkReturnStatements()
			else:
				# recursive call
				for child in node.children:
					self.checkReturnValues(child)
		except:
			printVerbose(self.errorListener.verbose, 2, "Exception caught in AST.checkReturnValues()", self.errorListener.file)
			
	def checkForErrors(self):
		'''
			Function to check if errors occured and return True if there were errors, False otherwise
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.checkForErrors()", self.errorListener.file)
	
		if self.errorListener.syntax_error_count > 0 or self.errorListener.type_error_count > 0:
			return True
		return False
		
	def buildSymbolTable(self):
		'''
			Function to build the symbol table by traversing the AST
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.buildSymbolTable()", self.errorListener.file)
		
		# Add all needed elements to the symboltable when traversing through the AST
		self.addScopes(self.root, self.scope)
		
		# Check all return values
		self.checkReturnValues(self.root)
		
		# Check if a main function is defined
		entry = self.scope.search("main")
		if not entry:
			# Error no main function defined so don't need to compile.
			self.errorListener.symbolTableError("expected main function to be defined in root, if not nothing should be compiled", self.root.start)
		elif entry["type"] != "function" and entry["defined"]:
			# Error no main function defined so don't need to compile.
			self.errorListener.symbolTableError("expected main function to be defined in root, if not nothing should be compiled", self.root.start)
		
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 2, "\nAST:" + str(self), self.errorListener.file)
		printVerbose(self.errorListener.verbose, 2, "\nSymbolTable:" + self.scope.prettyprint(indent = "    ") + "\n", self.errorListener.file)
		
		return self.checkForErrors()
		
	def getSymbolTable(self):
		'''
			Function to get the symbol table
		'''
		return self.scope
		
	def getFunction(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		for child in self.root.children:
			function = child.getFunction(identifier)
			if function:
				return function
		return None
		
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		for child in self.root.children:
			variable = child.getVariable(identifier)
			if variable:
				return variable
		return None
		
		
	def includeFunction(self, identifier):
		'''
			Helper function that checks if a function should be included into the code or not
			this should be any function except main scanf and printf
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.includeFunction()", self.errorListener.file)
		
		if identifier == "main":
			return False
		elif identifier == "scanf":
			return False
		elif identifier == "printf":
			return False
		return True
		
	def loadVariablesAndFunctions(self, params, file):
		'''
			The actual starting case, make room for all global variables and functions in the start of the program
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.loadVariablesAndFunctions()", self.errorListener.file)
		
		scope = self.root.scope
		
		# Get the right order of declaration
		ordered_variables = [ k for (v,k) in sorted([(v['local_order'], k) for (k,v) in scope.table.items() if 'type' in v and v['type'] == 'variable'])]
		
		# count the variables
		for entry in ordered_variables:
			dict = scope.table[entry]
			if dict["type"] == "variable":
				
				# Set location in symbol table
				dict["address"] = params["SP"]
				
				# set the SP
				if "subtype" in dict and dict["allocation"] == "static":
					params["SP"] = params["SP"] + dict["size"] + 1
				else:
					params["SP"] = params["SP"] + 1
		
		# return output for static size of data area
		if params["SP"] > 0:
			print("ssp " + str(params["SP"]), file=file)
			
		for entry in ordered_variables:
			dict = scope.table[entry]
			if dict["type"] == "variable":
				# Generate code because these are global variables
				node = self.getVariable(entry)
				node.generateCode(params, file)
			
		# jump to main
		print("ujp main", file=file)
		
		# handle inclusion of functions
		for entry in scope.table:
			dict = scope.table[entry]
			if dict["type"] == "function" and self.includeFunction(entry):
				node = self.getFunction(entry)
				node.generateCode(params, file)
		
	def codeGeneration(self, params, file):
		'''
			recursive calls for code generation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.codeGeneration()", self.errorListener.file)
			
		# Need to make room for all local variables
		self.loadVariablesAndFunctions(params, file)
		
		# Call code generation on main node
		node = self.getFunction("main")
		node.generateCode(params, file)
		
		# Code generation should end and so should the P-machine
		print("hlt", file=file)
		
		
	def generateCode(self, file):
		'''
			Start generating the code
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AST.generateCode()", self.errorListener.file)
		
		print("; Begin of generated code", file=file)
		
		# dictionary with parameters
		params = {}
		params["depth"] = 0
		params["SP"] = 0
		params["MP"] = 0
		params["label"] = 0
		
		self.codeGeneration(params, file)
		