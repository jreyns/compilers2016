
from symboltable import *
from errorhandling import *
from copy import deepcopy

EXCEPTION_VERBOSITY = 2


def printVerbose(current_level, needed_level, msg, file):
	if current_level >= needed_level:
		print(msg, file=file)

class ASTNode():

	richerthan = {"int": 2, "float":3, "char":1, "char*":2, "float*":2, "int*":2, "bool":2}

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		'''
			initializer
		'''
		# Added verbose levels for debugging
		printVerbose(errorListener.verbose, 3, "ASTNode.__init__()", errorListener.file)
		
		self.parent = parent
		self.depth = depth
		self.position = position
		self.type = type
		self.content = content
		self.children = []
		self.start = start
		self.end = end
		self.scope = None
		self.errorListener = errorListener
		
	def __repr__(self):
		'''
			Function which gets called by print
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.__repr__()", self.errorListener.file)
		
		content = ""
		if self.content != "":
			content = ", content: " + self.content
		temp = "\n"+ str("    " * self.depth) + str(self.start) + " " + self.type + content
		if self.children == []:
			return temp
		else:
			return temp + str(self.children)
		
	def addChild(self, depth, position, type = None, content = " ", start = None):
		'''
			Function to add a child to this node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.addChild()", self.errorListener.file)
		
		child = ASTNodeFactory.generateNode(self, depth, position, self.errorListener, type, content, start)
		self.children.append(child)
		return child
		
	def setType(self, type):
		'''
			Function to set the type of a node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.setType()", self.errorListener.file)
		
		self.type = type
		
	def setContent(self, content):
		'''
			Function to set the content of a node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.setContent()", self.errorListener.file)
		
		self.content = content
		
	def setParent(self, parent):
		'''
			Function to set the parent of a node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.setParent()", self.errorListener.file)
		
		self.parent = parent
		
	def setStart(self, start):
		'''
			Function to set the start of a ruleContext
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.setStart()", self.errorListener.file)
		
		self.start = start
		
	def setEnd(self, end):
		'''
			Function to set the end of a ruleContext
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.setEnd()", self.errorListener.file)
		
		self.end = end
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getLorR()", self.errorListener.file)
		
		if self.parent == None:
			return 2
		else:
			return self.parent.getLorR(self)
		
	def getID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getID()", self.errorListener.file)
		
		return ""
		
	def getFunctionID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getID()", self.errorListener.file)
		
		if self.parent == None:
			return ""
		else:
			return self.parent.getFunctionID()
		
	def reference(self, type):
		'''
			Function to reference a type
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.reference()", self.errorListener.file)
		
		return type + "*"
		
	def dereference(self, type):
		'''
			Function to dereference a type
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.dereference()", self.errorListener.file)
		
		if '*' in type:
			return type[:-1]
		else:
			#Error
			self.errorListener.symbolTableError("trying to dereference a variable which isn't a pointer", self.start, self.content)
			
	def deleteConst(self, type):
		'''
			Function to alter type for the richer than test
		'''
		if type and "const" in type:
			return type[5:]
			
		return type
		
	def translateCtoP(self, type):
		'''
			Function to translate types between C and P
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.translateCtoP()", self.errorListener.file)
		
		if type == "int":
			return "i"
		elif type == "float":
			return "r"
		elif type == "char":
			return "c"
		elif type == "bool":
			return "b"
		else:
			return "a"
			
	def matchType(self, type1, type2):
		'''
			Function to translate types between C and P
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.matchType()", self.errorListener.file)
		
		
		if type1 == "bool" and type2 == "bool":
			return "int"
		elif type1 == "bool":
			return type2
		elif type2 == "bool":
			return type1
		
		val1 = self.richerthan[type1]
		val2 = self.richerthan[type2]
		
		if val1 < val2:
			return type2
		else:
			return type1
			
	def checkType(self, type1, type2):
		'''
			Function to check the richer than property between two types
			type1 should be the LH
			type2 should be the RH
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.checkType()", self.errorListener.file)
				
		type1 = self.deleteConst(type1)
		type2 = self.deleteConst(type2)
		
		if not type1 or not type2:
			pass
		elif type1 == "void" or type2 == "void":
			if self.type != "return_statement":
				#error void should never be compared unless at a return statement
				self.errorListener.typeError("incompatible types when trying to convert " + type2 + " to " + type1, self.start, self.content)
				return False
		elif "*" in type1 and "*" in type2:
			if self.richerthan[type1[:-1]] < self.richerthan[type2[:-1]]:
				#warning, possible los of information
				self.errorListener.typeWarning("possible incompatible pointer types when converting " + type2 + " to " + type1, self.start, self.content)
		elif "*" in type1 or "*" in type2:
			#error trying to match array with something that isnt an array
			self.errorListener.typeWarning("possible error when trying to convert " + type2 + " to " + type1, self.start, self.content)
		elif self.richerthan[type1] < self.richerthan[type2]:
			#warning, possible los of information
			self.errorListener.typeWarning("possible loss of information when converting " + type2 + " to " + type1, self.start, self.content)
		return True
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope

		if self.type == "string":
			return "char*"

		
	def checkTypeMatch(self, scope = None, type_specifier = None):
		'''
			Function to check if LH and RH comply with the richer than rule
			else warnings should be given
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.checkTypeMatch()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
			
		types = []
		if type_specifier:
			types = [type_specifier, self.children[1].getType(scope)]
		else:
			types = [child.getType(scope) for child in self.children]
		self.checkType(types[0],types[1])
		
	def handleContinue(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.handleContinue()", self.errorListener.file)
		
		if self.parent != None:
			self.parent.handleContinue(params, file)
			
	def handleBreak(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.handleBreak()", self.errorListener.file)
		
		if self.parent != None:
			self.parent.handleBreak(params, file)
		
	def checkStatic(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.checkStatic()", self.errorListener.file)
		
		for child in self.children:
			if child.checkStatic():
				return True
			
		return False
		
	def getStaticSize(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getStaticSize()", self.errorListener.file)
		
		for child in self.children:
			if child.checkStatic():
				return child.getStaticSize()
			
		return 0
		
	def getArrayInfo(self, node, dict):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getArrayInfo()", self.errorListener.file)
		
		dict["size"] = 0
		dict["allocation"] = "dynamic"
		if self.checkStatic():
			dict["size"] = self.getStaticSize()
			dict["allocation"] = "static"
		return dict
		
	def getParameter(self, node, begin, end):
		'''
			Function to help get a single parameter out of a parameter list
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getParameter()", self.errorListener.file)
		
		param = {"type" : "variable"}
		for child in node.children[begin:end]:
			if child.type == "type_specifier":
				param["returns"] = child.content
			elif child.type == "basetype":
				param["returns"] = param["returns"] + child.content
			elif child.type == "array_index":
				param["returns"] = param["returns"] + "*"
				param["subtype"] = "array"
				param = self.getArrayInfo(self, param)
			elif child.type == "identifier":
				param["id"] = child.content
		return param
			
	def getParameters(self, node):
		'''
			Function to help get paramerters from a function
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getParameters()", self.errorListener.file)
		
		parameters = []
		params = [loc for loc in range(len(node.children)) if node.children[loc].type == "type_specifier"]
		for i,val in enumerate(params):
			if  i+1 < len(params):
				parameters.append(self.getParameter(node, val, params[i+1]))
			else:
				parameters.append(self.getParameter(node, val, len(node.children)))
		return parameters
		
	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getDeclaration()", self.errorListener.file)
		
		pass
		
	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.addScope():" + self.type, self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope

		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getFunction(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getFunction()", self.errorListener.file)
		
		for child in self.children:
			function = child.getFunction(identifier)
			if function:
				return function
		return None
		
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a variable_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getVariable()", self.errorListener.file)
		
		for child in self.children:
			variable = child.getVariable(identifier)
			if variable:
				return variable
		return None
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.generateCode()", self.errorListener.file)
		
		print("; " + self.type, file=file)
		
		for child in self.children:
			child.generateCode(params, file)
		
	def printNode(self):
		'''
			Function to print all information of a single node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.printNode()", self.errorListener.file)
		
		print("[",self.depth,self.position,self.type,self.content,"]")
		
	def printAll(self):
		'''
			Function to print the subtree of AST nodes with this node as root
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.printAll()", self.errorListener.file)
		
		self.print_node()
		[x.print_all() for x in self.children]
		
class IncludeNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(IncludeNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def getIncludes(self):
		'''
			Function to handle the include of stdio
			does nothing more than add the printf and scanf functions to the symbol table
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IncludeNode.getIncludes()", self.errorListener.file)
		
		return [("printf", {"type" : "function", "returns" : "void", "param0" : {"type" : "variable", "returns" : "char*"}, "param1" : {"type" : "variable", "returns" : "..."}, "defined" : True} ), 
		("scanf", {"type" : "function", "returns" : "void", "param0" : {"type" : "variable", "returns" : "char*"}, "param1" : {"type" : "variable", "returns" : "..."}, "defined" : True} )]

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IncludeNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Add includes, currently this only supports stdio,
			# which allows the use of printf and scanf
			includes = self.getIncludes()
			for id, include in includes:
				self.scope.declare(id, include)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in IncludeNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
			
class DeclarationNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(DeclarationNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def checkSameEntry(self, entry, dict):
		'''
			Function to check if two functions represent the function declaration and function definition
			returns True if allowed
			False otherwise
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.checkSameEntry()", self.errorListener.file)
		
		entry_keys = entry.keys()
		dict_keys = dict.keys()
		
		# keys should be the same, else different function
		if entry_keys != dict_keys:
			return False
			
		# check for amount and type of parameters (identifiers don't have to be present in declaration, they do in definition), 
		# defined should be False and True respectively
		# all other attributes should be the same
		for key in entry_keys:
			if "param" in key:
				if entry[key]["returns"] != dict[key]["returns"]:
					return False
			elif key == "defined":
				if (entry[key] or not dict[key]):
					return False
			elif entry[key] != dict[key]:
				return False
		return True

	def checkFunctionDefinition(self, scope, decl):
		'''
			Function to check if this function declaration is a function definition,
			this should be allowed if the function has only been declared and not defined
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.checkFunctionDefinition()", self.errorListener.file)
		
		(id, dict) = decl
		entry = scope.search(id)
		
		# Check if the declaration is the same
		return self.checkSameEntry(entry, dict)	

	def getDeclarations(self):
		'''
			Function to help the parsing of declarations
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.getDeclarations()", self.errorListener.file)
		
		ret = []
		type_specifier = self.children[0].content
		for child in self.children[1:]:
			child.scope = self.scope
			decl = child.getDeclaration(type_specifier)
			if decl != None:
				ret.append(decl)
		return ret

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Parse the declarations and add them to the symbol table
			declarations = self.getDeclarations()
			for decl in declarations:
				(id, dict) = decl
				if not self.scope.declare(id, dict):
					# variable or function has already been declared
					if self.checkFunctionDefinition(self.scope, decl):
						# Declaration appears to be a function definition, this is allowed if the function has only been declared
						self.scope.assign(id, dict)
					else:
						# Error should be thrown
						self.errorListener.symbolTableError(dict["type"] + id + " is already declared", self.start, self.content)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in DeclarationNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a variable_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.getVariable()", self.errorListener.file)
		
		for child in self.children:
			variable = child.getVariable(identifier)
			if variable:
				return self
		return None
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DeclarationNode.generateCode()", self.errorListener.file)
		
		try:
			# Parse the declarations and add them to the symbol table
			declarations = self.getDeclarations()
			for decl in declarations:
				(id, dict) = decl
				entry = self.scope.search(id)
				entry["declared"] = True
				
			for child in self.children:
				if child.type == "variable_definition" or child.type == "variable_declaration":
					child.generateCode(params, file)
				elif child.type == "variable_identifier":
					# Declaration of an array without assignment
					id = child.getID()
					entry = self.scope.search(id)
					address = entry["address"]
					if entry["allocation"] == "static":
						if not "relative" in entry:
							print("ldc a " + str(address), file=file)
							print("ldc a " + str(address + 1), file=file)
						else:
							print("lda " + str(params["depth"]) + " " + str(address), file=file)
							print("lda " + str(params["depth"]) + " " + str(address + 1), file=file)
						print("sto a", file=file)
					else:
						if not "relative" in entry:
							print("ldc a " + str(address), file=file)
						else:
							print("lda " + str(params["depth"]) + " " + str(address), file=file)
						child.children[1].generateCode(params, file)
						print("new", file=file)

				elif child.type == "function_definition" or child.type == "function_declaration":
					print("; although inline function definition and declarations are supported by the grammar, currently they are not inplemented", file=file)

		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in DeclarationNode.generateCode()", self.errorListener.file)

class FunctionDeclarationNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(FunctionDeclarationNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDeclarationNode.getDeclaration()", self.errorListener.file)
		
		# Of form basetype? identifier '(' parameter_list ')' so can have 2 or 3 children
		# handle all children in the appropriate way
		if len(self.children) == 3:
			type = type_specifier + self.children[0].content
			id = self.children[1].content
			params = self.getParameters(self.children[2])
			dict = {"type" : "function", "returns" : type, "defined" : False}
			for i in range(len(params)):
				dict["param" + str(i)] = params[i]
			return (id, dict)
		else :
			id = self.children[0].content
			params = self.getParameters(self.children[1])
			dict = {"type" : "function", "returns" : type_specifier, "defined" : False}
			for i in range(len(params)):
				dict["param" + str(i)] = params[i]
			return (id, dict)

class FunctionDefinitionNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(FunctionDefinitionNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def checkReturnStatements(self, type = None, node = None):
		'''
			Function which checks if return statements are present in this function definition
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.checkReturnStatements()", self.errorListener.file)
		
		count = 0
		
		# initialise correct values for recursive call
		if node == None:
			node = self
			id = self.getID()
			entry = self.scope.search(id)
			type = entry["returns"]
		
		# check return statements or check children
		if node.type == "return_statement":
			returnType = "void"
			if len(node.children) > 0:
				returnType = node.children[0].getType(self.scope)
			if node.checkType(returnType, type) and not (returnType == "void" and type == "void"):
				count += 1
		else:
			for child in node.children:
				count += self.checkReturnStatements(type, child)
		
		# provide output for recursive call or draw conclusions
		if self == node:
			if type != "void" and count == 0 and self.getID() != "main":
				self.errorListener.synError("expected return statement of type " + type + " in function " + self.children[0].content + " but none found", self.start, self.content)
			elif type == "void" and count > 0:
				self.errorListener.typeWarning("'return' with a value, in function returning void with identifier " + self.children[0].content, self.start, self.content)
		else:
			return count
			

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.getDeclaration()", self.errorListener.file)
		
		# Of form basetype? identifier '(' parameter_list ')' compound_statement so can have 3 or 4 children
		# handle all children in the appropriate way
		if len(self.children) == 4:
			type = type_specifier + self.children[0].content
			id = self.children[1].content
			params = self.getParameters(self.children[2])
			dict = {"type" : "function", "returns" : type, "defined" : True}
			for i in range(len(params)):
				dict["param" + str(i)] = params[i]
			return (id, dict)
		else :
			id = self.children[0].content
			params = self.getParameters(self.children[1])
			dict = {"type" : "function", "returns" : type_specifier, "defined" : True}
			for i in range(len(params)):
				dict["param" + str(i)] = params[i]
			return (id, dict)
		

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Parse the function definition
			# should be of form basetype? identifier (parameters) compound_statement
			
			# function definition so new scope is needed
			self.scope = scope.addChild(self.children[0].content + " type: " + self.type + " line: " + self.start)
			params = None
			
			# check if basetype is available and handle it properly
			if len(self.children) == 4:
				params = self.getParameters(self.children[2])
			else :
				params = self.getParameters(self.children[1])
				
			# add all parameters to the symbol table at current scope
			for param in params:
				id = param["id"]
				del param["id"]
				self.scope.declare(id, param)
				
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in FunctionDefinitionNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.getID()", self.errorListener.file)
		
		id = self.children[0].content
		if len(self.children) == 4:
			id = self.children[1].content
		return id
		
	def getFunctionID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ASTNode.getID()", self.errorListener.file)
		
		return self.getID()
		
	def getFunction(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.getFunction()", self.errorListener.file)
		
		id = self.getID()
		if id == identifier:
			return self
		return None
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionDefinitionNode.generateCode()", self.errorListener.file)
		
		print(self.getID() + ":", file=file)
		
		id = self.getID()
		
		# Save and reset stack position for generation of new values and restoring it after the call
		SP = params["SP"]
		
		if id != "main":
			params["SP"] = 5
		
		# Parameters should be declared at this point and relative addresses should be added
		entry = self.scope.search(id)
		parameters = {key:val for (key,val) in entry.items() if 'param' in key}
		for index in range(len(parameters)):
			param = parameters["param" + str(index)]
			param_id = param["id"]
			dict = self.scope.table[param_id]
			if dict["type"] == "variable":
				dict["declared"] = True
				if id != "main":
					dict["relative"] = True
				dict["address"] = params["SP"]
				params["SP"] = params["SP"] + 1
			
		self.children[-1].generateCode(params, file)
		
		
		if id != "main":
			print("retf", file=file)
			params["SP"] = SP
			
class CompoundNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(CompoundNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CompoundNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# compound statement so new scope should be added
			self.scope = scope.addChild(self.type +  " line: " + self.start)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in CompoundNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getVariables(self, params, id):
		'''
			Function to generate the reserved addresses for local variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CompoundNode.getVariables()", self.errorListener.file)
		
		# Get the right order of declaration
		ordered_variables = [ k for (v,k) in sorted([(v['local_order'], k) for (k,v) in self.scope.table.items() if 'type' in v and v['type'] == 'variable'])]
		
		# Save and stack position for generation of new values and restoring it after the call
		SP = params["SP"]
		
		# count the variables
		for entry in ordered_variables:
			dict = self.scope.table[entry]
			if dict["type"] == "variable":
				
				# Set location in symbol table
				dict["address"] = params["SP"]
				if id != "main":
					dict["relative"] = True
				
				# set the SP
				if "subtype" in dict and dict["allocation"] == "static":
					params["SP"] = params["SP"] + dict["size"] + 1
				else:
					params["SP"] = params["SP"] + 1
		
		# return output for static size of data area
		if params["SP"] - SP > 0:
			return "ssp " + str(params["SP"])
		return ""
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CompoundNode.generateCode()", self.errorListener.file)
		
		id = self.getFunctionID()
		content = self.getVariables(params, id)
		
		if content != "": 
			print(content, file=file)
		
		for child in self.children:
			child.generateCode(params, file)

class ConditionalNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ConditionalNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionalNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope

		types = [self.children[1].getType(scope), self.children[2].getType(scope)]
		return sorted(types)[1]

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionalNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# if test allows new scopes for both if and else statments
			self.scope = scope.addChild(self.type +  " line: " + self.start)
			for child in self.children[1:]:
				child.scope = self.scope.addChild(child.type)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in ConditionalNode.addScope()", self.errorListener.file)
		
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionalNode.getLorR()", self.errorListener.file)
		
		if self.children[0] == prev:
			return 1
		return 2
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionalNode.generateCode()", self.errorListener.file)
		
		# Need jumps so set label and hold value localy since params can be overwritten in child nodes
		params["label"] = params["label"] + 1
		label = str(params["label"])
		
		# Compair, if false then jump to else
		self.children[0].generateCode(params, file)
		type = self.children[0].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		if len(self.children) == 3:
			# there is an else to jump to
			print("fjp l" + label + "else", file=file)
		else:
			# no else so jump to end
			print("fjp l" + label + "end", file=file)
		
		# If
		print("l" + label + "if:", file=file)
		self.children[1].generateCode(params, file)
		
		# Else
		if len(self.children) == 3:
		
			# Need to jump after if statement past this else
			print("ujp l" + label + "end", file=file)
		
			# Make sure you are able to jump to this else statement
			print("l" + label + "else:", file=file)
			
			# Else code
			self.children[2].generateCode(params, file)
			
		# Add label to jump to from if
		print("l" + label + "end:", file=file)
					
class WhileNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(WhileNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "WhileNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# while statement so new scope should be added
			self.scope = scope.addChild(self.type +  " line: " + self.start)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in WhileNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "WhileNode.getLorR()", self.errorListener.file)
		
		if self.children[0] == prev:
			return 1
		return 2
		
	def handleContinue(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "WhileNode.handleContinue()", self.errorListener.file)
		
		# Jump to beginning of while
		print("ujp " + "l" + self.label + "while", file=file)
			
	def handleBreak(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "WhileNode.handleBreak()", self.errorListener.file)
		
		# Jump to end of while
		print("ujp " + "l" + self.label + "end", file=file)
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "WhileNode.generateCode()", self.errorListener.file)
		
		# Need jumps so set label and hold value localy since params can be overwritten in child nodes
		params["label"] = params["label"] + 1
		self.label = str(params["label"])
		
		# Initial while label to jump to
		print("l" + self.label + "while:", file=file)
		
		# Generate code for condition
		self.children[0].generateCode(params, file)
		type = self.children[0].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		
		# If false jump to end
		print("fjp " + "l" + self.label + "end", file=file)
		
		# Else handle code
		self.children[1].generateCode(params, file)
		
		# Jump to beginning of while
		print("ujp " + "l" + self.label + "while", file=file)
		
		# make sure to 
		print("l" + self.label + "end:", file=file)

class ForNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ForNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ForNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# for statement so new scope should be added
			self.scope = scope.addChild(self.type +  " line: " + self.start)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in ForNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ForNode.getLorR()", self.errorListener.file)
		
		if self.children[1] == prev:
			return 1
		return 2
		
		
	def handleContinue(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ForNode.handleContinue()", self.errorListener.file)
		
		# Jump to beginning of for
		print("ujp " + "l" + self.label + "inc", file=file)
			
	def handleBreak(self, params, file):
		'''
			Function to handle Continue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ForNode.handleBreak()", self.errorListener.file)
		
		# Jump to end of for
		print("ujp " + "l" + self.label + "end", file=file)
		
	def getVariables(self, params, id):
		'''
			Function to generate the reserved addresses for local variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CompoundNode.getVariables()", self.errorListener.file)
		
		# Get the right order of declaration
		ordered_variables = [ k for (v,k) in sorted([(v['local_order'], k) for (k,v) in self.scope.table.items() if 'type' in v and v['type'] == 'variable'])]
		
		# Save and stack position for generation of new values and restoring it after the call
		SP = params["SP"]
		
		# count the variables
		for entry in ordered_variables:
			dict = self.scope.table[entry]
			if dict["type"] == "variable":
				
				# Set location in symbol table
				dict["address"] = params["SP"]
				if id != "main":
					dict["relative"] = True
				
				# set the SP
				if "subtype" in dict and dict["allocation"] == "static":
					params["SP"] = params["SP"] + dict["size"] + 1
				else:
					params["SP"] = params["SP"] + 1
		
		# return output for static size of data area
		if params["SP"] - SP > 0:
			return "ssp " + str(params["SP"])
		return ""
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ForNode.generateCode()", self.errorListener.file)
		
		# Need jumps so set label and hold value localy since params can be overwritten in child nodes
		params["label"] = params["label"] + 1
		self.label = str(params["label"])
		
		content = self.getVariables(params, id)
		
		if content != "": 
			print(content, file=file)
		
		# Handle the assignment
		self.children[0].generateCode(params, file)
		
		# Initial for label to jump to
		print("l" + self.label + "for:", file=file)
		
		# Generate code for condition
		self.children[1].generateCode(params, file)
		type = self.children[1].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		
		# If false jump to end
		print("fjp " + "l" + self.label + "end", file=file)
		
		# Else handle code
		self.children[3].generateCode(params, file)
		
		# Increment label for continue
		print("l" + self.label + "inc:", file=file)
		
		# Handle increment
		self.children[2].generateCode(params,file)
		
		# Jump to beginning of for
		print("ujp " + "l" + self.label + "for", file=file)
		
		# make sure to 
		print("l" + self.label + "end:", file=file)

class ContinueNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ContinueNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ContinueNode.generateCode()", self.errorListener.file)
		
		self.handleContinue(params, file)
		
class BreakNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(BreakNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "BreakNode.generateCode()", self.errorListener.file)
		
		self.handleBreak(params, file)
		
class ExpressionListNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ExpressionListNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getParamCount(self):
		'''
			Function to get the right amount of parameters, this is needed for cup amount label
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ExpressionListNode.getParamCount()", self.errorListener.file)
		
		return len(self.children)

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ExpressionListNode.generateCode()", self.errorListener.file)
		
		id = self.getFunctionID()
		entry = self.scope.search(id)
		
		# Generate code for all parameters
		for index, child in enumerate(self.children):
			if child.type != "empty":
				param = entry["param" + str(index)]
				type = param["returns"]
				type1 = child.getType()
				child.generateCode(params, file)
				if type != type1:
					print("conv " + self.translateCtoP(type1) + " " + self.translateCtoP(type) , file=file)
		
class ReturnNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ReturnNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def checkReturnType(self):
		'''
			Function which checks if the return respects the richer than property
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReturnNode.checkReturnType()", self.errorListener.file)
			
		# found function definition in parents, check return type
		id = self.getFunctionID()
		entry = self.scope.search(id)
		returnType = entry["returns"]
		type = "void"
		if len(self.children) == 1:
			type = self.children[0].getType(self.scope)
		if returnType == "void" or id == "main":
			if self.children[0].type != "empty":
				self.children[0].type = "empty"
				if id != "main":
					self.errorListener.typeWarning("no return of type " + type + " expected in function with return type: void", self.start, self.content)
		else:
			return self.checkType(returnType, type)


	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReturnNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# return statement so check the richer than property compared to the expected return value
			type = "void"
			if len(self.children) > 0:
				type = self.children[0].getType(self.scope)
			self.checkReturnType()
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in ReturnNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReturnNode.generateCode()", self.errorListener.file)
		
		type = self.children[0].getType()
		self.children[0].generateCode(params, file)
		id = self.getFunctionID()
			
		if self.children[0].type != "empty":
			entry = self.scope.search(id)
			type1 = entry["returns"]
			if type != type1:
				print("conv " + self.translateCtoP(type) + " " + self.translateCtoP(type1) , file=file)
			print("str " + str(self.translateCtoP(type1)) + " " + str(params["depth"]) + " 0",file=file)
			
		
		if id != "main":
			print("retf",file=file)

class VariableDeclarationNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(VariableDeclarationNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDeclarationNode.getDeclaration()", self.errorListener.file)

		# Of form basetype? (variable_definition | variable_identifier)
		# Only seen if there is a basetype so handle all children accordingly
		for child in self.children:
			child.scope = self.scope
		return self.children[1].getDeclaration(type_specifier + self.children[0].content)	

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDeclarationNode.generateCode()", self.errorListener.file)
		
		# Get symbol table entry
		id = self.children[1].getID()
		entry = self.scope.search(id)
		if self.parent.type == "declaration":
			entry["declared"] = True
		address = entry["address"]
		
		self.children[1].generateCode(params, file)

class VariableDefinitionNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(VariableDefinitionNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getID(self):
		'''
			Helper function to get the identifier of lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.getID()", self.errorListener.file)
		
		return self.children[0].getID()
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.getLorR()", self.errorListener.file)
		
		return 1

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.getDeclaration()", self.errorListener.file)
		
		# definition of a variable so types should be checked for possible warnings
		# of form variable_identifier '=' simple_expression
		if len(self.children) > 0 and (self.children[0].type == "variable_identifier"):
			self.checkTypeMatch(scope = self.scope, type_specifier = type_specifier + "*")
			self.children[0].scope = self.scope
			return self.children[0].getDeclaration(type_specifier)
		self.checkTypeMatch(scope = self.scope, type_specifier = type_specifier)
		return (self.children[0].content , {"type" : "variable", "returns" : type_specifier})

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Handle array inits correctly
			if self.children[0].type == "variable_identifier" and self.children[1].type == "array_init":
				self.children[0].addScope(scope)
				id = self.children[0].getID()
				entry = scope.search(id)
				if entry != None and entry["allocation"] == "dynamic":
					entry["allocation"] = "static"
					entry["size"] = self.children[1].getStaticSize()
				elif entry != None and entry["size"] < self.children[1].getStaticSize():
					self.errorListener.typeWarning("excess elements in array initializer", self.start, self.content)
				self.children[1].addScope(scope)
				return[]
			elif self.children[0].type == "variable_identifier" and self.children[1].type == "string":
				self.children[0].addScope(scope)
				id = self.children[0].getID()
				entry = scope.search(id)
				if entry != None and entry["allocation"] == "dynamic":
					entry["allocation"] = "static"
					entry["size"] = self.children[1].getStaticSize()
				elif entry != None and entry["size"] < self.children[1].getStaticSize():
					self.errorListener.typeWarning("excess elements in array initializer", self.start, self.content)
				self.children[1].addScope(scope)
				return[]
			elif self.children[0].type == "variable_identifier" and self.children[1].type != "array_init":
				if self.children[1].type != "string" and self.children[0].getType(scope) != "char*":
					self.errorListener.typeError("invalid initializer", self.start, self.content)
		
			# if an variable_definition is encountered past the flatten then this means an assignment so check for conversion warnings
			self.checkTypeMatch(scope = scope)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in VariableDefinitionNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.getID()", self.errorListener.file)
		
		id = self.children[0].getID()
		return id
		
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.getVariable()", self.errorListener.file)
		
		id = self.getID()
		if id == identifier:
			return self
		return None
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableDefinitionNode.generateCode()", self.errorListener.file)
		
		# Get symbol table entry
		id = self.children[0].getID()
		entry = self.scope.search(id)
		if self.parent.type == "declaration":
			entry["declared"] = True
		address = entry["address"]
		
		if self.children[1].type == "array_init":
			params["id"] = id
			self.children[1].generateCode(params, file)
		elif self.children[1].type == "string":
			params["id"] = id
			self.children[1].generateCode(params, file)
		else:
			# Load the address of variable
			if not "relative" in entry:
				print("ldc a " + str(address), file=file)
			else:
				print("lda " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			
			# Load the value by generating the code to solve the simple expression
			self.children[1].generateCode(params, file)
			
			# Check types
			type = self.translateCtoP(entry["returns"])
			type1 = self.translateCtoP(self.children[1].getType())
			if type != type1:
				print("conv " + type1 + " " + type, file=file)
			
			# Store value in correct address
			print("sto " + type, file=file)

class FunctionCallNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(FunctionCallNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope

		id = self.children[0].getID()
		entry = scope.search(id)
		if entry:
			return entry["returns"]
			
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.getLorR()", self.errorListener.file)
		
		return 1
			
	def getFunctionID(self):
		'''
			Helper function to get the id
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.getID()", self.errorListener.file)
		
		return self.children[0].getID()
		
	def validParameters(self, entry):
		'''
			Function to check if a parameter is valid or not
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.validParameters()", self.errorListener.file)
		
		if self.children[1].type != "empty":
			node = self
			param_count = len(node.children) - 1
			children = node.children[1:]
			if self.children[1].type == "expression_list":
				node = self.children[1]
				param_count = len(node.children)
				children = node.children
				
			for i in range(param_count):
				param = entry["param" + str(i)]
				if not param:
					return False
				type = children[i].getType(self.scope)
				self.checkType(param["returns"], type)
				if "subtype" in param and param["subtype"] == "array":
					id = children[i].getID()
					dict = self.scope.search(id)
					if dict != None:
						if "subtype" in dict and param["allocation"] == "static" and dict["allocation"] == "static":
							if dict["size"] < param["size"]:
								self.errorListener.typeError("incompatible array sizes in function call", self.start, self.content)
						elif "subtype" in dict and param["allocation"] == "static" or dict["allocation"] == "static":
							if dict["size"] < param["size"]:
								self.errorListener.typeWarning("Possible incompatible array sizes in function call", self.start, self.content)

		return True

	def checkFunction(self):
		'''
			Function to check if a function call is allowed on this function
			in other words, has it already been declared?
			if so, does it have the right amount of parameters?
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.checkFunction()", self.errorListener.file)
		
		id = self.children[0].content
		entry = self.scope.search(id)
		if not entry:
			#Error function is called but not declared
			self.errorListener.symbolTableError("function is called but not declared", self.start, self.content)
		elif not self.validParameters(entry):
			#Error unsupported amount of parameters
			self.errorListener.symbolTableError("unsupported amount of parameters", self.start, self.content)

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Check if this is a valid function call
			# in other words check symbol table for entry and right amount of parameters
			self.checkFunction()
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in FunctionCallNode.addScope()", self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def getParamCount(self):
		'''
			Function to get the right amount of parameters, this is needed for cup amount label
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.getParamCount()", self.errorListener.file)
		
		if self.children[1].type == "empty":
			return 0
		elif self.children[1].type != "expression_list":
			return 1
		else:
			return self.children[1].getParamCount()
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FunctionCallNode.generateCode()", self.errorListener.file)
		
		# Generate stackframe
		print("mst " + str(params["depth"]), file=file)
		
		# Get the right function
		id = self.children[0].content
		entry = self.scope.search(id)
		
		# Generate code for all parameters
		for index, child in enumerate(self.children[1:]):
			if child.type != "empty":
				param = entry["param" + str(index)]
				type = param["returns"]
				type1 = child.getType()
				child.generateCode(params, file)
				if child.type != "expression_list"  and type != type1:
					print("conv " + self.translateCtoP(type1) + " " + self.translateCtoP(type) , file=file)
			
		# Jump to function
		print("cup " + str(self.getParamCount()) + " " + id, file=file)
		
class ReadNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ReadNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def translate(self, t):
		'''
			Function to translate scanf type to c type
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReadNode.translate()", self.errorListener.file)
		
		if t == "i":
			return "int"
		elif t == "f":
			return "float"
		elif t == "c":
			return "char"
		elif t == "s":
			return "char*"
		return None
		
		
	def getParams(self, string):
		'''
			Function to get parameters from a string
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReadNode.getParams():" + self.type, self.errorListener.file)
		
		try:
			# Pos of parameter
			pos = string.find('%')
			
			# Type of parameter
			type = self.translate(string[pos+1])
			
			return (type, self.children[1]);
				
		except:
			#Return Error, because string is of incorrect format
			self.errorListener.synError("unsupported format of string " + string, self.start, self.content)
			
	def checkParameters(self, params):
		'''
			Function to check parameters and arguments form scanf statements
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReadNode.checkParameters():" + self.type, self.errorListener.file)
		
		try:
			# Format of params
			(type, arg) = params
			
			if self.translateCtoP(type) != "a":
				type = self.reference(type)
			
			# Get type of argument
			id = arg.getID()
			entry = self.scope.search(id)
			arg_type = arg.getType(self.scope)
			
			if self.translateCtoP(arg_type) != "a":
				self.errorListener.typeError("expected argument of type address or pointer", self.start, self.content)
			
			# Check types
			self.checkType(type, arg_type)
			
		except:
			#Return Error, because string is of incorrect format
			self.errorListener.typeError("unsupported type conversion in scanf(" + type + "," + arg.content + ")", self.start, self.content)

		
	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReadNode.addScope():" + self.type, self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope

		# Get the format of what has to be read
		string = self.children[0].content
		
		# Get the parameters
		params = self.getParams(string)
		
		# Check the parameters
		self.checkParameters(params)
		
		return []
		
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ReadNode.generateCode()", self.errorListener.file)
		
		# Get the format of what has to be read
		string = self.children[0].content
		
		# Get the parameters
		(type, arg) = self.getParams(string)
		
		# Get type of argument
		id = arg.getID()
		entry = self.scope.search(id)
		arg_type = arg.getType(self.scope)
		if type != arg_type and self.reference(type) == arg_type:
			arg_type = self.dereference(arg_type)
		address = entry["address"]
		
		if not "subtype" in entry:
			print("ldc a " + str(address), file=file)
			print("in " + self.translateCtoP(type), file=file)
			if type != arg_type:
				print("conv " + self.translateCtoP(type) + " " + self.translateCtoP(arg_type), file=file)
			print("sto " + self.translateCtoP(arg_type), file=file)
		elif type == "char*":
			# Check if supported by Pmachine
			print("; please implement the read string in in Read Node, although not certain if it is supported by the Pmachine", file=file)
		
class PrintNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(PrintNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "PrintNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def translate(self, t):
		'''
			Function to translate scanf type to c type
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "PrintNode.translate()", self.errorListener.file)
		
		if t == "i":
			return "int"
		elif t == "f":
			return "float"
		elif t == "c":
			return "char"
		elif t == "s":
			return "char*"
		return None
		
	def getArguments(self):
		'''
			Function to get all arguments form a print node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "PrintNode.getArguments()", self.errorListener.file)
		
		dict = {}
		count = 0
		
		for child in self.children[1:]:
			dict[str(count)] = child
			count += 1
				
		return dict
		
	def getOutput(self):
		'''
			Function to convert an output string to individual char notations
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "PrintNode.generateCode()", self.errorListener.file)
		
		string = self.children[0].content[1:-1]
		arguments = self.getArguments()
		ret = []
		
		count = 0
		i = 0
		while i < len(string):
			if string[i] == "\\":
				if i+1 < len(string):
					ret.append(string[i] + string[i+1])
					i += 1
			elif string[i] == "%":
				if i+1 < len(string):
					param = self.translate(string[i+1])
					if param != None:
						type = self.translateCtoP(param)
						node = arguments[str(count)]
						ret.append((type, node))
						i += 1
						count += 1
					else:
						ret.append(string[i])
			else:
				ret.append(string[i])
			i += 1
		return ret
			
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "PrintNode.generateCode()", self.errorListener.file)
		
		output = self.getOutput()
		
		for char in output:
			if isinstance(char, str):
				print("ldc c '" + char + "'", file=file)
				print("out c", file=file)
			else:
				(type, node) = char
				type1 = self.translateCtoP(node.getType())
				node.generateCode(params, file)
				if type != type1:
					print("conv " + type1 + " " + type, file=file)
				print("out " + type, file=file)

class AssignmentNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(AssignmentNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AssignmentNode.getLorR()", self.errorListener.file)
		
		if self.children[0] == prev:
			return 0
		return 1
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AssignmentNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
			
		return self.children[0].getType(scope)
		
	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AssignmentNode.addScope():" + self.type, self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
			
		# Check if assignment isn't done on a const variable
		id = self.children[0].getID()
		entry = scope.search(id)
		
		if entry != None and "const" in entry["returns"]:
			self.errorListener.typeError("trying to write to a read-only variable", self.start, self.content)

		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def handleSpecialAssignment(self, params, file, operator):
		'''
			Function to handle += -= *= /= and %=
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AssignmentNode.handleSpecialAssignment()", self.errorListener.file)
		
		# need to get child 0 as a r value
		tempnode = self.children[1]
		self.children[1] = self.children[0]
		self.children[0] = tempnode
		
		type1 = self.children[1].getType()
		type2 = self.children[2].getType()
		type = self.translateCtoP(self.matchType(type1,type2))
		if type != "i" and type != "r":
			type = "i"
		type1 = self.translateCtoP(self.children[1].getType())
		type2 = self.translateCtoP(self.children[2].getType())
		
		self.children[1].generateCode(params, file)
		if type1 != type:
			print("conv " + type1 + " " + type, file=file)
		
		# might error if less than 3 children but should have been flattened in that scenario
		self.children[2].generateCode(params, file)
		if type2 != type:
			print("conv " + type2 + " " + type, file=file)
			
		if operator == "+=":
			print("add " + type, file=file)
		elif operator == "-=":
			print("sub " + type, file=file)
		elif operator == "*=":
			print("mul " + type, file=file)
		elif operator == "/=":
			print("div " + type, file=file)
		elif operator == "%=":
			# Do a - (b*(a/b)))
			# Convert second to int
			if type != "i":
				print("conv " + type + " i", file=file)
			
			# Load first again and conv to int
			self.children[1].generateCode(params, file)
			if type1 != "i":
				print("conv " + type1 + " i", file=file)
			
			# Load second again and convert to int
			self.children[2].generateCode(params, file)
			if type2 != "i":
				print("conv " + type1 + " i", file=file)
			
			# Integer div
			print("div i", file=file)
			
			# Integer mul
			print("mul i", file=file)
			
			# Integer sub
			print("sub i", file=file)
			
		if type1 != type:
			print("conv " + type + " " + type1, file=file)
			
		# need to swap back to not induce errors
		tempnode = self.children[1]
		self.children[1] = self.children[0]
		self.children[0] = tempnode
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AssignmentNode.generateCode()", self.errorListener.file)
		
		if self.children[0].type == "identifier":
			entry = self.scope.search(self.children[0].getID())
			if not "relative" in entry:
				if not "subtype" in entry:
					print("ldc a " + str(entry["address"]), file=file)
			else: 
				if not "subtype" in entry:
					print("lda " + str(params["depth"]) + " " + str(entry["address"]), file=file)
		else:
			self.children[0].generateCode(params, file)
				
		# Handle all possible operators
		operator = self.children[1].content
		
		# Check types
		type = self.children[0].getType()
		type1 = self.children[2].getType()
		
		if operator == "=":
			# Generate code for R value of assignment
			self.children[2].generateCode(params, file)
			
			if type != type1:
				print("conv " + self.translateCtoP(type1) + " " + self.translateCtoP(type), file=file)
		else:
			# handle += -= *= /= and %=
			self.handleSpecialAssignment(params, file, operator)
		
		# Store value
		print("sto " + self.translateCtoP(type), file=file)
			
		
		# Check if this assignment also serves as an r-value
		l_or_r_p = self.parent.getLorR(None)
		r_value = l_or_r_p == 1
		if r_value:
			node = self.children[0]
			if node.type == "variable_identifier" or node.type == "identifier":
				node.generateCode(params, file)
			else:
				for child in node.children:
					if child.type == "variable_identifier" or child.type == "identifier":
						child.generateCode(params, file)
						return
			
class EmptyNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(EmptyNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "EmptyNode.getType()", self.errorListener.file)
			
		return "void"
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "EmptyNode.generateCode()", self.errorListener.file)
		
		pass
	
class ConditionNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ConditionNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		type1 = self.children[1].getType(scope)
		type = type1
		
		if len(self.children) == 3:
			type2 = self.children[2].getType(scope)
			type = self.matchType(type1,type2)
			
		return type
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionNode.getLorR()", self.errorListener.file)
		
		if self.children[0] == prev:
			return 1
		return 2
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConditionNode.generateCode()", self.errorListener.file)
		
		# Need jumps so set label and hold value localy since params can be overwritten in child nodes
		params["label"] = params["label"] + 1
		self.label = str(params["label"])
		
		# Generate code for condition
		self.children[0].generateCode(params, file)
		type = self.children[0].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		
		# If false jump to false
		print("fjp " + "l" + self.label + "false", file=file)
		
		# True handle code
		self.children[1].generateCode(params, file)
		
		# Jump to end of condition
		print("ujp " + "l" + self.label + "end", file=file)
		
		# Initial while label to jump to
		print("l" + self.label + "false:", file=file)
		
		# False handle code
		self.children[2].generateCode(params, file)
		
		# make sure to 
		print("l" + self.label + "end:", file=file)
	
class DisjunctionNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(DisjunctionNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DisjunctionNode.getType()", self.errorListener.file)
		
		return "bool"

	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DisjunctionNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "DisjunctionNode.generateCode()", self.errorListener.file)
		
		# Generate code for first child
		self.children[0].generateCode(params, file)
		type = self.children[0].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		
		for child in self.children[1:]:
			# Generate code for next child
			child.generateCode(params, file)
			type = child.getType()
			if type != "bool":
				print("conv " + self.translateCtoP(type) + " b", file=file)
			
			# Flatten by taking or of two previous results
			print("or", file=file)

class ConjunctionNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ConjunctionNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConjunctionNode.getType()", self.errorListener.file)
		
		return "bool"

	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConjunctionNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ConjunctionNode.generateCode()", self.errorListener.file)
		
		# Generate code for first child
		self.children[0].generateCode(params, file)
		type = self.children[0].getType()
		if type != "bool":
			print("conv " + self.translateCtoP(type) + " b", file=file)
		
		for child in self.children[1:]:
			# Generate code for next child
			child.generateCode(params, file)
			type = child.getType()
			if type != "bool":
				print("conv " + self.translateCtoP(type) + " b", file=file)
			
			# Flatten by taking and of two previous results
			print("and", file=file)
			
class ComparisonNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ComparisonNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ComparisonNode.getType()", self.errorListener.file)
		
		return "bool"

	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ComparisonNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ComparisonNode.generateCode()", self.errorListener.file)
		
		type1 = self.children[0].getType()
		type2 = self.children[2].getType()
		type = self.translateCtoP(self.matchType(type1,type2))
		type1 = self.translateCtoP(self.children[0].getType())
		type2 = self.translateCtoP(self.children[2].getType())
		
		self.children[0].generateCode(params, file)
		if type1 != type:
			print("conv " + type1 + " " + type, file=file)
		
		# might error if less than 3 children but should have been flattened in that scenario
		self.children[2].generateCode(params, file)
		if type2 != type:
			print("conv " + type2 + " " + type, file=file)
			
		operator = self.children[1].content
		if operator == "==":
			print("equ " + type, file=file)
		elif operator == "!=":
			print("neq " + type, file=file)
	
class RelationNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(RelationNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "RelationNode.getType()", self.errorListener.file)
		
		return "bool"
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "RelationNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "RelationNode.generateCode()", self.errorListener.file)
		
		type1 = self.children[0].getType()
		type2 = self.children[2].getType()
		type = self.translateCtoP(self.matchType(type1,type2))
		type1 = self.translateCtoP(self.children[0].getType())
		type2 = self.translateCtoP(self.children[2].getType())
		
		self.children[0].generateCode(params, file)
		if type1 != type:
			print("conv " + type1 + " " + type, file=file)
		
		# might error if less than 3 children but should have been flattened in that scenario
		self.children[2].generateCode(params, file)
		if type2 != type:
			print("conv " + type2 + " " + type, file=file)
			
		operator = self.children[1].content
		if operator == "<":
			print("les " + type, file=file)
		elif operator == ">":
			print("grt " + type, file=file)
		elif operator == "<=":
			print("leq " + type, file=file)
		elif operator == ">=":
			print("geq " + type, file=file)
	
class SummationNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(SummationNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "SummationNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		type1 = self.children[0].getType(scope)
		type = type1
		
		if len(self.children) == 3:
			type2 = self.children[2].getType(scope)
			type = self.matchType(type1,type2)
		
		if type != "int" and type != "float":
			type = "int"
			
		return type
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "SummationNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "SummationNode.generateCode()", self.errorListener.file)
		
		type1 = self.children[0].getType()
		type2 = self.children[2].getType()
		type = self.translateCtoP(self.matchType(type1,type2))
		if type != "i" and type != "r":
			type = "i"
		type1 = self.translateCtoP(self.children[0].getType())
		type2 = self.translateCtoP(self.children[2].getType())
		
		self.children[0].generateCode(params, file)
		if type1 != type:
			print("conv " + type1 + " " + type, file=file)
		
		# might error if less than 3 children but should have been flattened in that scenario
		self.children[2].generateCode(params, file)
		if type2 != type:
			print("conv " + type2 + " " + type, file=file)
			
		operator = self.children[1].content
		if operator == "+":
			print("add " + type, file=file)
		elif operator == "-":
			print("sub " + type, file=file)

class TermNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(TermNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "TermNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		type1 = self.children[0].getType(scope)
		type = type1
		
		if len(self.children) == 3:
			type2 = self.children[2].getType(scope)
			type = self.matchType(type1,type2)
			
		if type != "int" and type != "float":
			type = "int"
			
		return type
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "TermNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "TermNode.generateCode()", self.errorListener.file)
		
		type1 = self.children[0].getType()
		type2 = self.children[2].getType()
		type = self.translateCtoP(self.matchType(type1,type2))
		if type != "i" and type != "r":
			type = "i"
		type1 = self.translateCtoP(self.children[0].getType())
		type2 = self.translateCtoP(self.children[2].getType())
		
		self.children[0].generateCode(params, file)
		if type1 != type:
			print("conv " + type1 + " " + type, file=file)
		
		# might error if less than 3 children but should have been flattened in that scenario
		self.children[2].generateCode(params, file)
		if type2 != type:
			print("conv " + type2 + " " + type, file=file)
		
		operator = self.children[1].content
		if operator == "*":
			print("mul " + type, file=file)
		elif operator == "/":
			print("div " + type, file=file)
		elif operator == "%":
			# Do a - (b*(a/b)))
			# Convert second to int
			if type != "i":
				print("conv " + type + " i", file=file)
			
			# Load first again and conv to int
			self.children[0].generateCode(params, file)
			if type1 != "i":
				print("conv " + type1 + " i", file=file)
			
			# Load second again and convert to int
			self.children[2].generateCode(params, file)
			if type2 != "i":
				print("conv " + type1 + " i", file=file)
			
			# Integer div
			print("div i", file=file)
			
			# Integer mul
			print("mul i", file=file)
			
			# Integer sub
			print("sub i", file=file)
			
class FactorNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(FactorNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FactorNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
			
		operator = self.children[0].content
		type = self.children[1].getType(scope)

		if operator == "-" and type != "int" and type != "float":
			type = "int"
		elif operator == "!":
			return "bool"
			
		return type
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FactorNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FactorNode.generateCode()", self.errorListener.file)
		
		self.children[1].generateCode(params, file)
		
		type = self.translateCtoP(self.children[1].getType())
		operator = self.children[0].content
		if operator == "!":
			print("conv " + type + " b", file=file)
			print("not", file=file)
		elif operator == "-":
			if type != "i" and type != "r":
				print("conv " + type + " i", file=file)
				type = "i"
			print("neg " + type, file=file)
			
class AtomNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(AtomNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AtomNode.getType()", self.errorListener.file)
		
		type = self.children[0].getType(scope)

		return type
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "AtomNode.generateCode()", self.errorListener.file)
		
		for child in self.children:
			child.generateCode(params, file)
			
class VariableIdentifierNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(VariableIdentifierNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getID(self):
		'''
			Helper function to get the identifier of lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.getID()", self.errorListener.file)
		
		if self.children[0].type == "identifier":
			return self.children[0].content
		else:
			return self.children[0].getID()
			
	def getNode(self):
		'''
			Helper function to get the lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.getNode()", self.errorListener.file)
		
		if self.children[0].type == "identifier":
			return self
		else:
			return self.children[0].getNode()
			
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.getVariable()", self.errorListener.file)
		
		id = self.getID()
		if id == identifier:
			return self
		return None
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		id = self.getID()
		entry = scope.search(id)
		
		if len(self.children) == 2:
			# Array
			return self.dereference(self.children[0].getType(scope))
		
		return self.children[0].getType(scope)

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.getDeclaration()", self.errorListener.file)
		
		# Of form identifier array_index? 
		dict = {"type" : "variable", "returns" : type_specifier + "*", "subtype" : "array"}
		return (self.children[0].content , self.getArrayInfo(self.children[1], dict))
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableIdentifierNode.generateCode()", self.errorListener.file)
		
		id = self.getID()
		node = self.getNode()
		entry = self.scope.searchDeclared(id)
		
		if len(node.children) == 1:
			node.children[0].generateCode(params, file)
		else:
			if self.parent.type == "declaration":
				entry = self.scope.search(id)
				entry["declared"] = True
				
			l_or_r = self.getLorR(None)

			if not "relative" in entry:
				print("ldo a " + str(entry["address"]), file=file)
			else:
				print("lod a " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			node.children[1].generateCode(params, file)
			type = node.children[1].getType()
			if type != "int":
				print("conv " + self.translateCtoP(type) + " i", file=file)
			print("ixa 1", file=file)
			
			if l_or_r == 1:
				print("ind " + self.translateCtoP(self.dereference(entry["returns"])), file=file)
				
class IdentifierNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(IdentifierNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getID(self):
		'''
			Helper function to get the identifier of lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.getID()", self.errorListener.file)
		
		return self.content
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope

		entry = scope.search(self.content)
		if entry:
			return entry["returns"]
			
	def getVariable(self, identifier):
		'''
			Function to get the astnode of a function_definition with a certain identifier
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.getVariable()", self.errorListener.file)
		
		id = self.getID()
		if id == identifier:
			return self
		return None

	def getDeclaration(self, type_specifier):
		'''
			Function which acts as a switch case to handle all different forms of declarations
			this function is allowed to be called recursively, SO BEWARE OF SCOPE LIVENESS
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.getDeclaration()", self.errorListener.file)
		
		# return identifier
		return (self.content , {"type" : "variable", "returns" : type_specifier})

	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.addScope()", self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
		try:
			# Check if this is a valid identifier
			# in other words check symbol table for entry
			if not (self.parent.type == "function_call" or self.parent.type == "function_definition") and not self.scope.search(self.content):
				#Return Error, because variable is not declared
				self.errorListener.symbolTableError("variable " + self.content + " is called but not declared", self.start, self.content)
		except:
			printVerbose(self.errorListener.verbose, EXCEPTION_VERBOSITY, "Exception caught in IdentifierNode.addScope()" , self.errorListener.file)
			
		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IdentifierNode.generateCode()", self.errorListener.file)
		
		id = self.content
		if self.parent.type == "declaration":
			entry = self.scope.search(id)
			entry["declared"] = True
		else:
			entry = self.scope.searchDeclared(id)

			if not "relative" in entry:
				print("ldo " + str(self.translateCtoP(entry["returns"])) + " " + str(entry["address"]), file=file)
			else:
				print("lod " + str(self.translateCtoP(entry["returns"])) + " " + str(params["depth"]) + " " + str(entry["address"]), file=file)

class IntegerNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(IntegerNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IntegerNode.getType()", self.errorListener.file)
		
		return "int"

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "IntegerNode.generateCode()", self.errorListener.file)
		
		print("ldc i " + self.content, file=file)
		
class FloatNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(FloatNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FloatNode.getType()", self.errorListener.file)
		
		return "float"

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "FloatNode.generateCode()", self.errorListener.file)
		
		print("ldc r " + self.content, file=file)
		
class CharNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(CharNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CharNode.getType()", self.errorListener.file)
		
		return "char"

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "CharNode.generateCode()", self.errorListener.file)
		
		print("ldc c " + self.content, file=file)

class StringNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(StringNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.getType()", self.errorListener.file)
		
		return "char*"
		
	def cleanInit(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.cleanInit()", self.errorListener.file)
			
		return list(self.content[1:-1])
		
	def checkStatic(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.checkStatic()", self.errorListener.file)
			
		return True
		
	def getStaticSize(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.getStaticSize()", self.errorListener.file)
		
		return len(self.cleanInit())
		
	def getOutput(self):
		'''
			Function to convert an output string to individual char notations
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.generateCode()", self.errorListener.file)
		
		return list(self.content[1:-1])
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "StringNode.generateCode()", self.errorListener.file)
		
		id = params.pop("id")
		entry = self.scope.search(id)
		type = self.translateCtoP(self.dereference(entry["returns"]))
		address = entry["address"]
		
		if not "relative" in entry:
			print("ldc a " + str(address), file=file)
			print("ldc a " + str(address + 1), file=file)
		else:
			print("lda " + str(params["depth"]) + " " + str(address), file=file)
			print("lda " + str(params["depth"]) + " " + str(address + 1), file=file)
		print("sto a", file=file)
		
		output = self.getOutput()
		for index,child in enumerate(output):
			if index < entry["size"]:
				if not "relative" in entry:
					print("ldc a " + str(address + index + 1), file=file)
				else:
					print("lda " + str(params["depth"]) + " " + str(address + index + 1), file=file)
					
				print("ldc c '" + child + "'", file=file)
				
				type1 = "c"
				
				if type != type1:
					print("conv " + type1 + " " + type, file=file)
				print("sto " + type, file=file)
		
class VariableNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(VariableNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getID(self):
		'''
			Helper function to get the identifier of lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.getID()", self.errorListener.file)
		
		for child in self.children:
			if child.type != "pre" and child.type != "post":
				return child.getID()
				
	def getNode(self):
		'''
			Helper function to get the lowest node
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.getNode()", self.errorListener.file)
		
		if self.children[0].type == "pre":
			if self.children[1].type != "identifier":
				return self.children[1].getNode()
		else:
			if self.children[0].type != "identifier":
				return self.children[0].getNode()
		return self
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		if self.children[0].content == "&":
			entry = scope.search(self.children[1].getID())
			if entry:
				return self.reference(entry["returns"])
		elif self.children[0].content == "*":
			entry = scope.search(self.children[1].getID())
			if entry:
				return self.dereference(entry["returns"])
		elif self.children[0].type == "pre":
			entry = scope.search(self.children[1].getID())
			if entry:
				return entry["returns"]
		else :
			entry = scope.search(self.children[0].getID())
			if entry:
				return entry["returns"]
				
	def correctValue(self, scope, Lvalue):
		'''
			Function to check for correct usage of Lvalue
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.correctValue():" + self.type, self.errorListener.file)
		
		if scope == None:
			scope = self.scope
			
		id = self.children[1].getID()
		if self.children[1].type == "post":
			id = self.children[0].getID()
		entry = scope.search(id)
		type = self.translateCtoP(entry["returns"])
		
		if Lvalue:
			return self.children[0].content == "*" and type == "a"
		return self.children[0].content == "*"
				
	def addScope(self, scope):
		'''
			Function to allow recursive addition of symbol table entries
			
			this function will add the right AST node references to the right depth in the symbol table tree representation
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.addScope():" + self.type, self.errorListener.file)
		
		# Add a scope to the current node.
		# This can be needed during recursive calls
		if self.scope == None:
			self.scope = scope
			
		'''
			Variables can be handled according to 3 different scenario's
			0:
				l-value of an assignment (only * as pre is allowed)
			1:
				both l and r-value in right of assignment, variable definition, print, array init or array index
				where the value of the result has to be pushed on to the stack
			2:
				all other cases where some operators should be handled but stack should be popped
		'''
		l_or_r = self.getLorR(None)
		
		if l_or_r == 0:
			if not self.correctValue(scope, True):
				self.errorListener.synError("expected identifier or dereference of pointer in left hand side of assignment", self.start, self.content)
		elif l_or_r == 1:
			if self.children[0].content == "&":
				id = self.children[1].getID()
				entry = scope.search(id)
				if entry != None and "const" in entry["returns"]:
					self.errorListener.typeWarning("initialization discards 'const' qualifier from pointer target type", self.parent.start, self.parent.content)
		elif len(self.children) == 3:
			if not self.correctValue(scope, False):
				self.errorListener.synError("incompatible pre and post operators", self.start, self.content)

		# standard return which should happen if all children should be added to the scope in the recursive calls
		return self.children
				
	def handleDeref(self, params, file, node, l_or_r):
		'''
			Function to handle dereference operator on variable
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleDeref()", self.errorListener.file)
		
		# Load object at location of node
		type = self.dereference(node.getType())
		entry = None
		
		if node.type == "identifier":
			entry = self.scope.searchDeclared(node.content)
		else:
			id = node.getID()
			node = node.getNode()
			entry = self.scope.searchDeclared(id)
			
		if l_or_r == 0:
			# l value so return value
			if not "subtype" in entry:
				if not "relative" in entry:
					print("ldo a " + str(entry["address"]), file=file)
				else: 
					print("lda " + str(params["depth"]) + " " + str(entry["address"]), file=file)
		elif l_or_r == 1:
			# r value so return value
			if not "subtype" in entry:
				if not "relative" in entry:
					print("ldo a " + str(entry["address"]), file=file)
					print("ind " + self.translateCtoP(type)  , file=file)
				else: 
					print("lda " + str(params["depth"]) + " " + str(entry["address"]), file=file)
					print("ind " + self.translateCtoP(type)  , file=file)
		elif l_or_r == 2:
			#do nothing
			pass
		elif l_or_r == 3:
			# r value but address is already on stack
			if not "subtype" in entry:
				if not "relative" in entry:
					print("ind " + self.translateCtoP(type)  , file=file)
				else: 
					print("ind " + self.translateCtoP(type)  , file=file)
			
	def handleRef(self, params, file, node, l_or_r):
		'''
			Function to handle reference operator on variable
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleRef()", self.errorListener.file)
		
		# Load address of node
		type = self.reference(node.getType())
		entry = None
		
		if l_or_r == 1:
			if node.type == "identifier":
				entry = self.scope.searchDeclared(node.content)
			else:
				id = node.getID()
				node = node.getNode()
				entry = self.scope.searchDeclared(id)
				
			if not "subtype" in entry:
				if not "relative" in entry:
					print("ldc a " + str(entry["address"]), file=file)
				else: 
					print("lda " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			elif entry["subtype"] == "array":
				if not "relative" in entry:
					print("ldo a " + str(entry["address"]), file=file)
				else:
					print("lod a " + str(params["depth"]) + " " + str(entry["address"]), file=file)
				node.children[1].generateCode(params, file)
				type = node.children[1].getType()
				if type != "int":
					print("conv " + self.translateCtoP(type) + " i", file=file)
				print("ixa 1", file=file)
			
	def handleAdd(self, params, file, node):
		'''
			Function to handle ++ operator on variable
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleAdd()", self.errorListener.file)
		
		# Load value and increment
		type = node.getType()
		entry = None
		
		if node.type == "identifier":
			entry = self.scope.searchDeclared(node.content)
		else:
			id = node.getID()
			node = node.getNode()
			entry = self.scope.searchDeclared(id)
			
		if not "subtype" in entry:
			if not "relative" in entry:
				print("ldo " + self.translateCtoP(type) + " " + str(entry["address"]), file=file)
			else: 
				print("lod " + self.translateCtoP(type) + " " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			# Add one
			print("inc " + self.translateCtoP(type) + " 1", file=file)
			if not "relative" in entry:
				print("sro " + self.translateCtoP(type) + " " + str(entry["address"]), file=file)
			else: 
				print("str " + self.translateCtoP(type) + " " + str(params["depth"]) + " " + str(entry["address"]), file=file)
		elif entry["subtype"] == "array":
			if not "relative" in entry:
				print("ldo a " + str(entry["address"]), file=file)
			else:
				print("lod a " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			node.children[1].generateCode(params, file)
			type = node.children[1].getType()
			if type != "int":
				print("conv " + self.translateCtoP(type) + " i", file=file)
			print("ixa 1", file=file)
			print("dpl a", file=file)
			print("ind " + self.translateCtoP(self.dereference(entry["returns"])), file=file)
			
			# Add one
			print("inc " + self.translateCtoP(type) + " 1", file=file)
			
			# Save
			print("sto " + self.translateCtoP(type), file=file)
			
	def handleSub(self, params, file, node):
		'''
			Function to handle ++ operator on variable
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleSub()", self.errorListener.file)
		
		# Load value and increment
		type = node.getType()
		entry = None
		
		if node.type == "identifier":
			entry = self.scope.searchDeclared(node.content)
		else:
			id = node.getID()
			node = node.getNode()
			entry = self.scope.searchDeclared(id)
			
		if not "subtype" in entry:
			if not "relative" in entry:
				print("ldo " + self.translateCtoP(type) + " " + str(entry["address"]), file=file)
			else: 
				print("lod " + self.translateCtoP(type) + " " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			# Subtract one
			print("dec " + self.translateCtoP(type) + " 1", file=file)
			if not "relative" in entry:
				print("sro " + self.translateCtoP(type) + " " + str(entry["address"]), file=file)
			else: 
				print("str " + self.translateCtoP(type) + " " + str(params["depth"]) + " " + str(entry["address"]), file=file)
		elif entry["subtype"] == "array":
			if not "relative" in entry:
				print("ldo a " + str(entry["address"]), file=file)
			else:
				print("lod a " + str(params["depth"]) + " " + str(entry["address"]), file=file)
			node.children[1].generateCode(params, file)
			type = node.children[1].getType()
			if type != "int":
				print("conv " + self.translateCtoP(type) + " i", file=file)
			print("ixa 1", file=file)
			print("dpl a", file=file)
			print("ind " + self.translateCtoP(self.dereference(entry["returns"])), file=file)
			
			# Add one
			print("dec " + self.translateCtoP(type) + " 1", file=file)
			
			# Save
			print("sto " + self.translateCtoP(type), file=file)
				
	def handlePre(self, params, file, l_or_r):
		'''
			Function to handle pre operators on variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handlePre()", self.errorListener.file)
		
		operator = ""
		node = None
		
		for child in self.children:
			if child.type == "pre":
				operator = child.content
			elif child.type == "post":
				pass
			else:
				node = child
				
		if operator == "*":
			self.handleDeref(params, file, node, l_or_r)
		elif operator == "&":
			self.handleRef(params, file, node, l_or_r)	
		elif operator == "++":
			self.handleAdd(params, file, node)
			if l_or_r == 1:
				node.generateCode(params, file)
		elif operator == "--":
			self.handleSub(params, file, node)
			if l_or_r == 1:
				node.generateCode(params, file)
		
	def handlePost(self, params, file, l_or_r):
		'''
			Function to handle post operators on variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handlePost()", self.errorListener.file)	
		
		operator = ""
		node = None
		
		for child in self.children:
			if child.type == "pre":
				pass
			elif child.type == "post":
				operator = child.content
			else:
				node = child
				
		if operator == "++":
			if l_or_r == 1:
				node.generateCode(params, file)
			self.handleAdd(params, file, node)
		elif operator == "--":
			if l_or_r == 1:
				node.generateCode(params, file)
			self.handleSub(params, file, node)
			
	def handleLValue(self, params, file, l_or_r):
		'''
			General function to handle pre and post operators on variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleLValue()", self.errorListener.file)
		
		# should only occur when dereferencing a variable
		if self.children[0].type == "pre" and self.children[0].content == "*":
			self.handleDeref(params, file, self.children[1], l_or_r)
				
	def handleOperators(self, params, file, l_or_r):
		'''
			General function to handle pre and post operators on variables
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.handleOperators()", self.errorListener.file)
		
		# Handle different amount of children
		child_count = len(self.children)
		if child_count == 3:
			#handle post before pre
			self.handlePost(params, file, 1)
			self.handlePre(params, file, 3)
		elif child_count == 2:
			#handle pre or post
			if self.children[0].type == "pre":
				self.handlePre(params, file, l_or_r)
			else:
				self.handlePost(params, file, l_or_r)

	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.generateCode()", self.errorListener.file)
		
		'''
			Variables can be handled according to 3 different scenario's
			0:
				l-value of an assignment (only * as pre is allowed)
			1:
				both l and r-value in right of assignment, variable definition, print, array init or array index
				where the value of the result has to be pushed on to the stack
			2:
				all other cases where some operators should be handled but stack should be popped
		'''
		l_or_r = self.getLorR(None)
		
		if l_or_r == 0:
			self.handleLValue(params, file, l_or_r)
		elif l_or_r == 1:
			self.handleOperators(params, file, l_or_r)
		elif l_or_r == 2:
			self.handleOperators(params, file, l_or_r)

class ArrayIndexNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ArrayIndexNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		return self.children[0].getType(scope)
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def checkStatic(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.checkStatic()", self.errorListener.file)
			
		return self.children[0].type == "integer"
		
	def getStaticSize(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.getStaticSize()", self.errorListener.file)
		
		if self.checkStatic():
			return int(self.children[0].content) 
			
		return 0
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.generateCode()", self.errorListener.file)
		
		for child in self.children:
			child.generateCode(params, file)
		
class ArrayInitNode(ASTNode):

	def __init__(self, parent, depth, position, errorListener, type = None, content = " ", start = None, end = None):
		super(ArrayInitNode, self).__init__(parent, depth, position, errorListener, type, content, start, end)
		
	def getType(self, scope = None):
		'''
			Function which acts as a switch to get teh type of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "VariableNode.getType()", self.errorListener.file)
		
		if scope == None:
			scope = self.scope
		
		type = ""
		
		if len(self.children) > 0:
			type = self.children[0].getType(scope)
			 
		for child in self.children:
			type1 = child.getType(scope)
			if type1 != type:
				self.errorListener.typeError("incompatible types " + type + " and " + type1 + " in array initializer", self.start, self.content)
			 
		return self.reference(type)
		
	def cleanInit(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.cleanInit()", self.errorListener.file)
			
		init = self.content[1:-1]
		return init.split(',')
		
	def checkStatic(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.checkStatic()", self.errorListener.file)
			
		return True
		
	def getStaticSize(self):
		'''
			Function to help find the value of an expression
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayIndexNode.getStaticSize()", self.errorListener.file)
			
		return len(self.cleanInit())
		
	def getLorR(self, prev):
		'''
			Helper function to check wether a child variable has to behave as an l or r value
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayInitNode.getLorR()", self.errorListener.file)
		
		return 1
		
	def generateCode(self, params, file):
		'''
			Function to generate the appropriate code blocks
		'''
		# Added verbose levels for debugging
		printVerbose(self.errorListener.verbose, 3, "ArrayInitNode.generateCode()", self.errorListener.file)
		
		id = params.pop("id")
		entry = self.scope.search(id)
		type = self.translateCtoP(self.dereference(entry["returns"]))
		address = entry["address"]
		
		if not "relative" in entry:
			print("ldc a " + str(address), file=file)
			print("ldc a " + str(address + 1), file=file)
		else:
			print("lda " + str(params["depth"]) + " " + str(address), file=file)
			print("lda " + str(params["depth"]) + " " + str(address + 1), file=file)
		print("sto a", file=file)
		
		
		for index,child in enumerate(self.children):
			if index < entry["size"]:
				if not "relative" in entry:
					print("ldc a " + str(address + index + 1), file=file)
				else:
					print("lda " + str(params["depth"]) + " " + str(address + index + 1), file=file)
					
				child.generateCode(params, file)
				
				type1 = self.translateCtoP(child.getType())
				
				if type != type1:
					print("conv " + type1 + " " + type, file=file)
				print("sto " + type, file=file)
		
class ASTNodeFactory:

	@staticmethod
	def generateNode(parent, depth, position, errorListener, type = None, content = None, start = None):
		'''
			Function to automatically generate a node of a certain type
		'''
		# Added verbose levels for debugging
		printVerbose(errorListener.verbose, 3, "ASTNodeFactory.generateNode()", errorListener.file)

		if type == "include":
			return IncludeNode(parent, depth, position, errorListener, type, content, start)
		elif type == "declaration":
			return DeclarationNode(parent, depth, position, errorListener, type, content, start)
		elif type == "function_declaration":
			return FunctionDeclarationNode(parent, depth, position, errorListener, type, content, start)
		elif type == "function_definition":
			return FunctionDefinitionNode(parent, depth, position, errorListener, type, content, start)
		elif type == "compound_statement":
			return CompoundNode(parent, depth, position, errorListener, type, content, start)
		elif type == "conditional_statement":
			return ConditionalNode(parent, depth, position, errorListener, type, content, start)
		elif type == "while_statement":
			return WhileNode(parent, depth, position, errorListener, type, content, start)
		elif type == "for_statement":
			return ForNode(parent, depth, position, errorListener, type, content, start)
		elif type == "continue_statement":
			return ContinueNode(parent, depth, position, errorListener, type, content, start)
		elif type == "break_statement":
			return BreakNode(parent, depth, position, errorListener, type, content, start)
		elif type == "return_statement":
			return ReturnNode(parent, depth, position, errorListener, type, content, start)
		elif type == "expression_list":
			return ExpressionListNode(parent, depth, position, errorListener, type, content, start)
		elif type == "variable_declaration":
			return VariableDeclarationNode(parent, depth, position, errorListener, type, content, start)
		elif type == "variable_definition":
			return VariableDefinitionNode(parent, depth, position, errorListener, type, content, start)
		elif type == "function_call":
			return FunctionCallNode(parent, depth, position, errorListener, type, content, start)
		elif type == "read_statement":
			return ReadNode(parent, depth, position, errorListener, type, content, start)
		elif type == "print_statement":
			return PrintNode(parent, depth, position, errorListener, type, content, start)
		elif type == "assignment":
			return AssignmentNode(parent, depth, position, errorListener, type, content, start)
		elif type == "empty":
			return EmptyNode(parent, depth, position, errorListener, type, content, start)
		elif type == "condition":
			return ConditionNode(parent, depth, position, errorListener, type, content, start)
		elif type == "disjunction":
			return DisjunctionNode(parent, depth, position, errorListener, type, content, start)
		elif type == "conjunction":
			return ConjunctionNode(parent, depth, position, errorListener, type, content, start)
		elif type == "comparison":
			return ComparisonNode(parent, depth, position, errorListener, type, content, start)
		elif type == "relation":
			return RelationNode(parent, depth, position, errorListener, type, content, start)
		elif type == "summation":
			return SummationNode(parent, depth, position, errorListener, type, content, start)
		elif type == "term":
			return TermNode(parent, depth, position, errorListener, type, content, start)
		elif type == "factor":
			return FactorNode(parent, depth, position, errorListener, type, content, start)
		elif type == "atom":
			return AtomNode(parent, depth, position, errorListener, type, content, start)
		elif type == "variable_identifier":
			return VariableIdentifierNode(parent, depth, position, errorListener, type, content, start)
		elif type == "identifier":
			return IdentifierNode(parent, depth, position, errorListener, type, content, start)
		elif type == "integer":
			return IntegerNode(parent, depth, position, errorListener, type, content, start)
		elif type == "floating_point":
			return FloatNode(parent, depth, position, errorListener, type, content, start)
		elif type == "char":
			return CharNode(parent, depth, position, errorListener, type, content, start)
		elif type == "string":
			return StringNode(parent, depth, position, errorListener, type, content, start)
		elif type == "variable":
			return VariableNode(parent, depth, position, errorListener, type, content, start)
		elif type == "array_index":
			return ArrayIndexNode(parent, depth, position, errorListener, type, content, start)
		elif type == "array_init":
			return ArrayInitNode(parent, depth, position, errorListener, type, content, start)
		else:
			return ASTNode(parent, depth, position, errorListener, type, content, start)