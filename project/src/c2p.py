import sys
from antlr4 import *
from visitor import astVisitor
from smallCLexer import smallCLexer
from smallCParser import smallCParser
from ast import *
from errorhandling import *

import argparse


def main(argv):
	argparser = argparse.ArgumentParser(description='Compile SmallC programs to P machine code.')
	argparser.add_argument('infile', nargs = 1, help='Input filenames.', type=str)
	argparser.add_argument('outfile', nargs = '?', help='Output filename for P machine code, defaults to sys.stdout', default = sys.stdout, type=str)

	argparser.add_argument('-e', '--err', help='Verbose output filename, defaults to sys.stderr', dest = 'err', default = sys.stderr, type=str)
	
	arg_verbosity = argparser.add_mutually_exclusive_group()
	arg_verbosity.add_argument( '-v', '--verbose', help='verbosity level by number, e.g. --verbose 3 gives verbosity 3', default=1, type=int)
	arg_verbosity.add_argument( '-q', '--quiet', action='store_const', const = 0, dest = 'verbose', default = 1, help='sets verbosity to 0')
	arg_verbosity.add_argument( '-s', '--silent', action='store_const', const = 0, dest = 'verbose', default = 1, help='sets verbosity to 0')
	args = argparser.parse_args(argv[1:])
	
	outfile = open(args.outfile, 'w') if args.outfile != sys.stdout else sys.stdout
	err = open(args.err[0], 'w') if args.err != sys.stderr else sys.stderr
	cfile = args.infile[0]
	
	if args.verbose > 1:
		print("verbosity level: ", args.verbose, file=err)

	if args.verbose > 2:
		print("current file:", cfile, outfile, file=err)
		
	# setup some stuff
	input = FileStream(cfile)
	lexer = smallCLexer(input)
	stream = CommonTokenStream(lexer)
	parser = smallCParser(stream)
	
	# set verbosity
	MyErrorListener.INSTANCE.setVerbosity(args.verbose)
	MyErrorListener.INSTANCE.setOutputFile(err)

	# inject our own error handling
	lexer.removeErrorListeners()
	lexer.addErrorListener(MyErrorListener.INSTANCE)
	parser.removeErrorListeners()
	parser.addErrorListener(MyErrorListener.INSTANCE)

	# run everything
	tree = parser.program()
	a = AST(MyErrorListener.INSTANCE)
	visitor = astVisitor(a)
	tree.accept(visitor)
	
	# start code generation of no errors found
	if not a.buildSymbolTable():
		a.generateCode(outfile)
	
	if args.verbose > 1:
		print("\nParsing ended with " + str(MyErrorListener.INSTANCE.syntax_error_count) + " syntax errors and " + str(MyErrorListener.INSTANCE.warning_count) + " warnings.")
	
	return MyErrorListener.INSTANCE.syntax_error_count
	
if __name__ == '__main__':
	sys.exit(main(sys.argv))