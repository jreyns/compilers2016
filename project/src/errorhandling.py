from antlr4 import *
import sys
from antlr4.error.ErrorListener import ErrorListener
from antlr4.error.ErrorListener import ConsoleErrorListener
from antlr4.error.ErrorStrategy import ErrorStrategy
from antlr4.error.ErrorStrategy import DefaultErrorStrategy



class MyErrorListener(ConsoleErrorListener):
	verbose = 1 # level of verbosity
	
	file = sys.stderr
	
	syntax_error_count = 0
	
	warning_count = 0
	
	type_error_count = 0
	
	def setVerbosity(self, verb):
		self.verbose = verb;
		
	def setOutputFile(self, file):
		self.file = file

	def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
		self.syntax_error_count += 1
		if self.verbose > 2:
			print("MyErrorListener.syntaxError", file=self.file)
		if self.verbose > 0:
			print("line " + str(line) + ":" + str(column) + " " + msg, file=self.file)

	def reportAmbiguity(self, recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs):
		if self.verbose > 2:
			print("MyErrorListener.reportAmbiguity", file=self.file)
		return super().reportAmbiguity(recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs)

	def reportAttemptingFullContext(self, recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs):
		if self.verbose > 2:
			print("MyErrorListener.reportAttemptingFullContext", file=self.file)
		return super().reportAttemptingFullContext(recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs)

	def reportContextSensitivity(self, recognizer, dfa, startIndex, stopIndex, prediction, configs):
		if self.verbose > 2:
			print("MyErrorListener.reportContextSensitivity", file=self.file)
		return super().reportContextSensitivity(recognizer, dfa, startIndex, stopIndex, prediction, configs)
		
	def synError(self, msg, location, offendingSymbol = None):
		self.syntax_error_count += 1
		if self.verbose > 2:
			print("MyErrorListener.synError", file=self.file)
		if self.verbose > 0:
			print("line " + location + " " + msg + ((" at input " + offendingSymbol) if offendingSymbol else ""), file=self.file)
		
	def symbolTableError(self, msg, location, offendingSymbol = None):
		self.syntax_error_count += 1
		if self.verbose > 2:
			print("MyErrorListener.symbolTableError", file=self.file)
		if self.verbose > 0:
			print("line " + location + " " + msg + ((" at input " + offendingSymbol) if offendingSymbol else ""), file=self.file)

	def typeWarning(self, msg, location, offendingSymbol = None):
		self.warning_count += 1
		if self.verbose > 2:
			print("MyErrorListener.typeWarning", file=self.file)
		if self.verbose > 0:
			print("warning: line " + location + " " + msg + ((" at input " + offendingSymbol) if offendingSymbol else ""), file=self.file)
			
	def typeError(self, msg, location, offendingSymbol = None):
		self.type_error_count += 1
		if self.verbose > 2:
			print("MyErrorListener.typeError", file=self.file)
		if self.verbose > 0:
			print("line " + location + " " + msg + ((" at input " + offendingSymbol) if offendingSymbol else ""), file=self.file)

MyErrorListener.INSTANCE = MyErrorListener()

class MyErrorStrategy(DefaultErrorStrategy):
	verbose = 1 # level of verbosity
	
	def setVerbosity(self, verb):
		self.verbose = verb;

	def reset(self, recognizer:Parser):
		if self.verbose > 2:
			print("MyErrorStrategy.reset")
		return super().reset(recognizer)

	def recoverInline(self, recognizer:Parser):
		if self.verbose > 2:
			print("MyErrorStrategy.recoverInline")
		return super().recoverInline(recognizer)

	def recover(self, recognizer:Parser, e:RecognitionException):
		if self.verbose > 2:
			print("MyErrorStrategy.recover")
		return super().recover(recognizer, e)

	def sync(self, recognizer:Parser):
		if self.verbose > 2:
			print("MyErrorStrategy.sync")
		return super().sync(recognizer)
		
	def inErrorRecoveryMode(self, recognizer:Parser):
		if self.verbose > 2:
			print("MyErrorStrategy.inErrorRecoveryMode")
		return super().inErrorRecoveryMode(recognizer)

	def reportError(self, recognizer:Parser, e:RecognitionException):
		if self.verbose > 2:
			print("MyErrorStrategy.reportError")
		return super().reportError(recognizer, e)

		
if __name__ == "__main__":
	# Add your error listener to the lexer if required
	lexer.removeErrorListeners()
	parser.removeErrorListeners()
	lexer.addErrorListener(MyErrorListener)
	parser.addErrorListener(MyErrorListener)
	parser._errHandler = MyErrorStrategy()