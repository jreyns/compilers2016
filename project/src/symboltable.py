class Scope:
	def __repr__(self):
		base = "\nScope:"
		name = "" 		if   self.printid == None 	else " scope name: " + str(self.printid)
		data = "" 		if     self.table == {} 	else " data: " + str(self.table)
		children = "" 	if (self.children == []) 	else " children: " + str(self.children)
		return base + name + data + children

	def prettyprint(self, depth = 0, indent = "  "):
		indentstr = "\n" + (depth * indent)
		base = indentstr + "Scope"
		name = ":" 		if   self.printid == None 	else " name: " + str(self.printid)
		data = "" 		if     self.table == {}		else "".join([indentstr +" " + str(key) + ":" + str(val) for key,val in self.table.items()])
		children = "" 	if (self.children == []) 	else (depth * indent) + "".join([child.prettyprint(depth + 1, indent) for child in self.children])
		return base + name + data + children
		
	def printtree(self, prettyprint = False):
		if self.parent == None:
			if prettyprint:
				return self.prettyprint()
			else:
				return self.__repr__()
		return self.parent.printtree(prettyprint)

	def __init__(self, printid = None, parent = None, previous = None):
		'''
		Default initialisation with everything == None is good enough for a root node.
		'''
		self.local_order = 0
		self.printid = printid
		self.table = {}
		self.parent = parent
		self.previous = previous
		if previous != None:
			print("Usage of siblings is deprecated.")
		self.children = []
		
	def addChild(self, printid = None):
		'''
		If this node already has children, add the last one as previous sibling.
		'''
		if len(self.children) > 0:
			self.children.append(Scope(printid, self))
			#self.children.append(Scope(printid, self, self.children[-1])) # deprecated sibling-using version
		else:
			self.children.append(Scope(printid, self))
		return self.children[-1]
		
	def addSibling(self, printid = None):
		'''
		If the parent is the root node, create a new root node.
		Either way, add the new node to the parent node.
		'''
		if self.parent == None:
			self.parent = Scope()
			self.parent.children.append(self)
		return self.parent.addChild(printid)
		
	""" # gone, not needed
	def findInSiblings(self, searchid):
		'''
		If it's this node: return it.
		If not, find it in our predecessor nodes.
		'''
		if searchid in self.table:
			return self
		elif self.previous == None:
			return None
		else:
			return self.previous.findInSiblings(searchid)
	"""
	
	def findInScope(self, searchid, declared=False):
		'''
		If it's in this level of nodes, return it.
		If not, search the parent level.
		'''
		if (searchid in self.table) and ((not declared) or (("declared" in self.table[searchid]) and (self.table[searchid]["declared"] == True))):
			return self
		elif self.parent == None:
			return None
		else:
			return self.parent.findInScope(searchid, declared)
	
	def declare(self, id, data = {}):
		'''
		When something is declared, we need to look up if it was already 
		declared in this scope (siblings only) and if not, we create it and 
		possibly fill in the info in one go.
		'''
		temp = self.findInScope(id)
		if temp == None: # if it didn't exist yet
			if 'type' in data and data['type'] == 'variable':
				data['local_order'] = self.local_order
				self.local_order += 1
			self.table[id] = data
			return True
		else:
			return False
	
	def assign(self, id, data):
		'''
		For an assignment we need to find the id somewhere in the scope,
		if found: assign data, if not, error.
		'''
		temp = self.findInScope(id) # temp is either None if not found or the node with the correct id
		if temp == None:
			return False
		else:
			temp.table[id].update(data)
			return True
			
	def searchDeclared(self, id):
		'''
		'''
		temp = self.findInScope(id, True)
		if temp == None:
			return None
		return temp.table[id]
		
	def search(self, id):
		'''
		'''
		temp = self.findInScope(id)
		if temp == None:
			return None
		return temp.table[id]

if __name__ == "__main__":
	sc1 = Scope("root")
	ch1 = sc1.addChild("ch1")
	ch2 = sc1.addChild("ch2")
	ch1.declare("x", [("type", "float"), 23.5])
	ch3 = sc1.addChild("ch3")
	ch4 = sc1.addChild("ch4")
	subch1 = ch1.addChild("subch1")
	ch4.assign("x", [("type", "apple"), 2])
	print("printing:")
	print(sc1)
	print(ch4.findInScope("x"))
	print("prettyprinting:")
	print(sc1.prettyprint())
