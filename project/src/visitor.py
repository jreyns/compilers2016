import sys
from antlr4 import *
from smallCVisitor import smallCVisitor
if __name__ is not None and "." in __name__:
	from .smallCParser import smallCParser
else:
	from smallCParser import smallCParser
from ast import *

# This class defines a complete generic visitor for a parse tree produced by smallCParser.

class astVisitor(smallCVisitor):
					
		
	def __init__(self, ast):
		self.ast = ast
		self.ruleNames = smallCParser.ruleNames
		
	def mayFlatten(self, n, ctx):
		'''
			Function to test if this node only has one meaningful child node and isn't a terminal node
		'''
		return (n == 1 or (n == 2 and ctx.getChild(1).getText() == ';')) and not isinstance(ctx.getChild(0), tree.Tree.TerminalNodeImpl)
	
	def flatten(self, ctx):
		'''
			Function to flatten a part of the parsetree
			
			should only flatten nodes with a single meaningful child node
		'''
		n = ctx.getChildCount()
		if self.mayFlatten(n, ctx):
			return self.flatten(ctx.getChild(0))
		return ctx
		
	def visitChildren(self, node):
		'''
			Function overloaded from visitors to allow flattening
		'''
		result = self.defaultResult()
		n = node.getChildCount()
		for i in range(n):
			if not self.shouldVisitNextChild(node, result):
				return
			c = node.getChild(i)
			# added line to push meaningful information up in the tree
			# this is an optimalisation based on the grammar
			# since the grammar allows a large depth increase from condition to atom
			new_node = self.flatten(c)
			new_node.parentCtx = node
			childResult = new_node.accept(self)
			result = self.aggregateResult(result, childResult)
		return result
		
	def addNode(self, ctx, content = ""):
		'''
			Helper function to streamline node generation in the AST
		'''
		start = str(ctx.start.line) + ":" + str(ctx.start.column)
		self.ast.addChild(ctx.depth(), start, self.ruleNames[ctx.getRuleIndex()], content)
		
		
		
	'''
		Rest of the file are node specific visitor functions based on the visitors from antlr4
	'''
	
	# Visit a parse tree produced by smallCParser#program.
	def visitProgram(self, ctx:smallCParser.ProgramContext):
		start = str(ctx.start.line) + ":" + str(ctx.start.column)
		self.ast.setRoot(start, self.ruleNames[ctx.getRuleIndex()], "")
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#include.
	def visitInclude(self, ctx:smallCParser.IncludeContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#declaration.
	def visitDeclaration(self, ctx:smallCParser.DeclarationContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#function_declaration.
	def visitFunction_declaration(self, ctx:smallCParser.Function_declarationContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#function_declaration.
	def visitFunction_definition(self, ctx:smallCParser.Function_definitionContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)

		
	# Visit a parse tree produced by smallCParser#function_call.
	def visitFunction_call(self, ctx:smallCParser.Function_callContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#parameter_list.
	def visitParameter_list(self, ctx:smallCParser.Parameter_listContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#parameter_declaration.
	def visitParameter_decl_list(self, ctx:smallCParser.Parameter_decl_listContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#compound_statement.
	def visitCompound_statement(self, ctx:smallCParser.Compound_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#conditional_statement.
	def visitConditional_statement(self, ctx:smallCParser.Conditional_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#while_statement.
	def visitWhile_statement(self, ctx:smallCParser.While_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#for_statement.
	def visitFor_statement(self, ctx:smallCParser.For_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)

	# Visit a parse tree produced by smallCParser#return_statement.
	def visitReturn_statement(self, ctx:smallCParser.Return_statementContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#read_statement.
	def visitRead_statement(self, ctx:smallCParser.Read_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#print_statement.
	def visitPrint_statement(self, ctx:smallCParser.Print_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#statement.
	def visitStatement(self, ctx:smallCParser.StatementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#statement.
	def visitContinue_statement(self, ctx:smallCParser.Continue_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#statement.
	def visitBreak_statement(self, ctx:smallCParser.Break_statementContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#expression_list.
	def visitExpression_list(self, ctx:smallCParser.Expression_listContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#expression.
	def visitExpression(self, ctx:smallCParser.ExpressionContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#expression.
	def visitAssignment(self, ctx:smallCParser.AssignmentContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#expression.
	def visitAssignment_operator(self, ctx:smallCParser.Assignment_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#expression.
	def visitEmpty(self, ctx:smallCParser.EmptyContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#simple_expression.
	def visitSimple_expression(self, ctx:smallCParser.Simple_expressionContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#condition.
	def visitCondition(self, ctx:smallCParser.ConditionContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#disjunction.
	def visitDisjunction(self, ctx:smallCParser.DisjunctionContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#conjunction.
	def visitConjunction(self, ctx:smallCParser.ConjunctionContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#comparison.
	def visitComparison(self, ctx:smallCParser.ComparisonContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#comparison_operator.
	def visitComparison_operator(self, ctx:smallCParser.Comparison_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#relation.
	def visitRelation(self, ctx:smallCParser.RelationContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#relational_operator.
	def visitRelational_operator(self, ctx:smallCParser.Relational_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#summation.
	def visitSummation(self, ctx:smallCParser.SummationContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#term_operator.
	def visitTerm_operator(self, ctx:smallCParser.Term_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#term.
	def visitTerm(self, ctx:smallCParser.TermContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#factor_operator.
	def visitFactor_operator(self, ctx:smallCParser.Factor_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#factor.
	def visitFactor(self, ctx:smallCParser.FactorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)

	# Visit a parse tree produced by smallCParser#factor_operator.
	def visitUnary_operator(self, ctx:smallCParser.Unary_operatorContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#atom.
	def visitAtom(self, ctx:smallCParser.AtomContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#atom.
	def visitInteger(self, ctx:smallCParser.IntegerContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#atom.
	def visitFloating_point(self, ctx:smallCParser.Floating_pointContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	# Visit a parse tree produced by smallCParser#atom.
	def visitChar(self, ctx:smallCParser.CharContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#variable.
	def visitVariable(self, ctx:smallCParser.VariableContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#pre.
	def visitPre(self, ctx:smallCParser.PreContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#post.
	def visitPost(self, ctx:smallCParser.PostContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#array_index.
	def visitArray_index(self, ctx:smallCParser.Array_indexContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#array_init.
	def visitArray_init(self, ctx:smallCParser.Array_initContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)

		
	# Visit a parse tree produced by smallCParser#type_specifier.
	def visitType_specifier(self, ctx:smallCParser.Type_specifierContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		

	# Visit a parse tree produced by smallCParser#type_cast.
	def visitType_cast(self, ctx:smallCParser.Type_castContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#variable_declaration.
	def visitVariable_declaration(self, ctx:smallCParser.Variable_declarationContext):
		self.addNode(ctx)
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#variable_definition.
	def visitVariable_definition(self, ctx:smallCParser.Variable_definitionContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#variable_identifier.
	def visitVariable_identifier(self, ctx:smallCParser.Variable_identifierContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#basetype.
	def visitBasetype(self, ctx:smallCParser.BasetypeContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#identifier.
	def visitIdentifier(self, ctx:smallCParser.IdentifierContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)


	# Visit a parse tree produced by smallCParser#string.
	def visitString(self, ctx:smallCParser.StringContext):
		self.addNode(ctx, ctx.getText())
		return self.visitChildren(ctx)
		
	