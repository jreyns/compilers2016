@echo off

call .\clean.bat
call .\build.bat

if not exist ".\build\c" mkdir .\build\c

copy /y .\c_code\* .\build\c
copy /y .\gcc_tests\* .\build\c
copy /y .\c_torture\* .\build\c
copy /y .\c_code_input\* .\build\c
copy /y .\c_code_nocompile\* .\build\c 


cd .\build

for %%f in (.\c\*.c) do (
	echo %%~f
	py -3 c2p.py %%~f %%~f.p %*
)

cd ..
