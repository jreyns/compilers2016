#!/bin/bash

./clean.sh
./build.sh

if [ ! -d "./build/c" ]; then
  mkdir ./build/c
  mkdir ./build/p
fi 


cp ./c_code/* ./build/c
cp ./c_code_nocompile/* ./build/c
cp ./c_code_input/* ./build/c
cp ./gcc_tests/* ./build/c
cp ./c_torture/* ./build/c
cp ./Pmachine/Pmachine ./build/p

cd ./build

echo Compiling p code

for F in ./c/*.c
do
	echo $F
	python3 c2p.py $F $F.p
	if [[ -s $F.p ]] ; then
		echo "$F.p has data."
	else
		rm -rf $F.p
		echo "$F.p is empty, removed."
	fi ;

done

echo Moving generated p code to ./build/p
mv ./c/*.p ./p



cd ..
