failure, should compile in ansi C but doesn't because following are not supported:
 1. functions without return type (returns int in ansi C)
 2. void as function argument (e.g. int main(void) {}) is not accepted
 3. pointers to pointers
 4. ^ operator not supported
 5. gets, malloc & free are not supported
 6. binary & operator not supported
 7. combining pre- and postfix operators not supported
  
success:
 1. everything goes fine (no output)
 2. called but not declared
 3. loss of information warning
 4. expects type, but doesn't get any (semi-vague error: no viable alternative)
 5. no return value in function returning non-void
 6. return value in function returning void 
 7. warning: "return;" in function returning void
 
								v no errors (success 1)
									v errors, as expected (success > 1)
										v known inconsistencies with C (failure)
20000609-1.c					success: 1
20021219-1.c							failure: 3
20030416-1.c						success: 2
20140326-1.c						success: 3
386.c									failure: 1
991229-3.c							success: 2
acker1.c						success: 1
add386.c							success: 4
alter_array.c					success: 1
array.c								success: 3
array_expanded.c				success: 1
bt.c							success: 1
call.c								success: 5
call386.c						success: 1
check_palindrome.c						failure: 5
check_palindrome_for.c					failure: 5
conversions.c						success: 3
ddd.c							success: 1
error_nameless_namespace.c			success: 2
factor_operators.c				success: 1
faculteit.c						success: 1
fibonacci.c						success: 1
floyds_triangle.c				success: 1
floyds_triangle_for.c			success: 1
fnul.c									failure: 6
foo.c									failure: 7
gcov-10.c								failure: 6
gcov-11.c								failure: 6
gcov-16.c							success: 8
gcov-2.c						success: 1
gcov-5b.c						success: 1
gcov-8.c						success: 1
i++.c							success: 1
l.c								success: 1
linkage-x.c						success: 1
pr49712.c						success: 1
pr53409.c						success: 1
pr58340.c						success: 1
pr61222-1.c								failure: 4
precedence.c					success: 1
pre_and_post.c					success: 1
quickscopetest.c				success: 1
references.c					success: 1
scal.c							success: 1
sieve.c							success: 1
sim.c							success: 1
sort2.c									failure: 6
speed.c							success: 1
syntax_error.c						success: 4
test-loop.c							success: 2
time.c							success: 1
v.c								success: 1
